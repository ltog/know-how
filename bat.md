# bat

## Installation

Ubuntu:

```bash
sudo apt install bat

# make batcat available as bat
mkdir -p ~/.local/bin
ln -s /usr/bin/batcat ~/.local/bin/bat
fish_add_path /home/l/.local/bin/
```

## Customization

Create config file:

```bash
mkdir -p (dirname (bat --config-file))
touch (bat --config-file)
vim (bat --config-file)
```


Use a theme suitable for bright terminal themes:

```bash
bat --list-themes # show themes
echo '--theme="GitHub"' >> (bat --config-file)
```

# Windows

## Create calc shortcut

If you have a "Calc" key on your keyboard that opens the windows calculator, you can replace it and run an interactive Python shell instead (with the `math` module already imported):

Use this registry patch:

```
Windows Registry Editor Version 5.00

[HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\AppKey\18]
"ShellExecute"="ubuntu run python3 -ic 'import math; import math as m'"
```

Requirements:

- Windows 10 (may run on earlier versions too)
- Installed Ubuntu (WSL)
- Python3 installed on Ubuntu

Source: https://superuser.com/a/1096520
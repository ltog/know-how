# ripgrep

## Common use-cases

Search case-insensitive:

    rg -i GEOS

Search fixed string:

    rg -F 'myfixedstring'

Show lines before/after match:

    rg [-A 2 -B 3 | -C 3]

Search only certain filetypes:

    rg mystring -tphp

Search NOT certain filetypes:

    rg mystring -Tphp

Limit output length:

    rg -M 80

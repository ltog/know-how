# Copy files from docker container to host

    docker cp <containerId>:/path/in/container/oldfile /host/path/newfile

`<containerId>` is determined by `docker ps -alq` oder `docker ps -a`.

Source: <https://stackoverflow.com/a/22050116>

# Vagrant know-how

## Technical terms

- **Provider**: Technology to create a virtual machine, e.g. VirtualBox, VMware, Hyper-V, Docker, etc.
- **Provisioner**: Technology to configure a virtual machine, e.g. shell (script), Ansible (remote), Ansible local
- **Box**: Basic VM that has to be downloaded only once and then will be used as base VM for every virtual machine. Saved under `C:/Users/USERNAME/.vagrant.d/boxes` (Windows) or `~/.vagrant.d/boxes` (Mac OS X, Linux) Boxes are always created for exactly one provider.

## Basic handling of boxes/VMs

### How to setup a new virtual machine

- Install VirtualBox from https://www.virtualbox.org/
- Install Vagrant from https://www.vagrantup.com/downloads.html
- Create a new Vagrantfile: `vagrant init ubuntu/jammy64`
- Adjust Vagrantfile
- Create virtual machine: `vagrant up`
- Add SSH config info: `vagrant ssh-config >> /home/l/.ssh/config`

### How to work with the virtual machine

- Open the virtual machine using the VirtualBox GUI (User/Pass: `vagrant:vagrant`)
- Open an SSH connection `vagrant ssh`

### How to apply changes made to the Vagrantfile

`vagrant provision`

### How to remove a virtual machine

`vagrant destroy`

### How to remove a box

`vagrant box remove ubuntu/bionic64`

## Adjusting a Vagrantfile

### Hardware parameters (specific to VirtualBox)

```ruby
  config.vm.provider "virtualbox" do |vb|
      vb.memory = "8192"
      vb.cpus = "4"
  end
```

### root shell commands

Adjust the created Vagrantfile at the line starting with `config.vm.provision "shell",`

```ruby
  config.vm.provision "shell", inline: <<-SHELL
    apt-get update
    apt-get install -y apache2
  SHELL
```


### normal user shell commands

Variant 1:

```ruby
  config.vm.provision "shell", inline: <<-SHELL
    sudo -i -u vagrant curl -fsSL get.docker.com -o get-docker.sh
    sh get-docker.sh
    usermod -aG docker vagrant
  SHELL
```

Variant 2:

```ruby
  $userscript = <<-SCRIPT
  # ... commands to be run as user vagrant ...
  SCRIPT

  config.vm.provision "shell", inline: $userscript, privileged: false
```

from: https://stackoverflow.com/a/22556979


### Using the Ansible local Provider

Add the following lines to the Vagrant file:

```ruby
  config.vm.provision "ansible_local" do |ansible|
    ansible.playbook = "playbook.yml"
  end
```

Create a file `playbook.yml` with a content such as:

```yaml
- name: playbook that could possibly do many things but currently only sets the hostname
  hosts: all
  become: yes
  tasks:
  - name: Set new hostname
    hostname: name=my_new_hostname
```

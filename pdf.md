# PDF

## Combining multiple slide sets

```bash
pdftk *.pdf cat output combined.pdf
```

## Removing text

```bash
sudo apt install qpdf
qpdf --qdf file.pdf - > file.qdf
sed '/unwanted_text/d' > modified_file.qdf
fix-qdf modified_file.qdf > fixed_modified_file.qdf
qpdf --compress-streams=y --stream-data=compress --recompress-flate --compression-level=9 --object-streams=generate fixed_modified_file.qdf modified_file_compressed.pdf # optionally use --linearize (makes file a little bigger)
```

## PDF Cropping

Goal: Remove white borders from PDF files

### pdfcrop

- + Easy installation: `sudo apt-get install texlive-extra-utils`
- + Scriptable: `pdfcrop input.pdf output.pdf`
- - Large installation: 232MB
- - PDF size increases with factor ~7
- - PDF internal links will be lost

Source: https://askubuntu.com/questions/124692/command-line-tool-to-crop-pdf-files

### briss

- + Installation: `snap install briss` (?)
- - Crashes at startup:

```
$ briss
/snap/briss/3/bin/desktop-launch: line 222: 16466 Segmentation fault      (core dumped) $RUNTIME/usr/lib/$ARCH/gdk-pixbuf-2.0/gdk-pixbuf-query-loaders > $GDK_PIXBUF_MODULE_FILE
/snap/briss/3/bin/desktop-launch: line 285: 16545 Segmentation fault      (core dumped) $RUNTIME/usr/lib/$ARCH/libgtk2.0-0/gtk-query-immodules-2.0 > $GTK_IM_MODULE_FILE
en_US.UTF-8
/snap/briss/3/run.sh: line 13: 16547 Segmentation fault      (core dumped) java -version
Error: failed /snap/briss/3/usr/lib/jvm/java-8-openjdk-amd64/jre/lib/amd64/server/libjvm.so, because (null)
```

### https://pdfresizer.com/crop

Seems to use `pdfcrop`

### krop

- Installation: `snap install krop`
- http://arminstraub.com/software/krop
- + PDF size doesn't grow
- - Internal links will be lost


# Linux

## Viewing large (log)files

Start output at byte 500 (counting starts at 1):

    tail -c +500


## Finding large files

    sudo find / -type f -size +100M -print0 | xargs -0 du -h

# diff-so-fancy

## Installation

```fish
sudo add-apt-repository ppa:aos1/diff-so-fancy
sudo apt update
sudo apt install diff-so-fancy

git config --global color.ui true

# use color codes from https://en.wikipedia.org/wiki/ANSI_escape_code#8-bit
git config --global color.diff-highlight.oldNormal    "160 bold"
git config --global color.diff-highlight.oldHighlight "124 bold 217"
git config --global color.diff-highlight.newNormal    "34 bold"
git config --global color.diff-highlight.newHighlight "28 bold 193"

git config --global color.diff.meta       "172"
git config --global color.diff.frag       "magenta bold"
git config --global color.diff.func       "146 bold"
git config --global color.diff.commit     "172 bold"
git config --global color.diff.old        "160 bold"
git config --global color.diff.new        "34 bold"
git config --global color.diff.whitespace "160 reverse"

# Should the first block of an empty line be colored. (Default: true)
git config --bool --global diff-so-fancy.markEmptyLines false

# Should the pesky + or - at line-start be removed. (Default: true)
# this is needed to see newly added empty lines
git config --bool --global diff-so-fancy.stripLeadingSymbols false

git config --global core.pager "diff-so-fancy | less --tabs=4 -RFX"
git config --global interactive.diffFilter "diff-so-fancy --patch"

# alias for fish
alias dsf="diff-so-fancy"
funcsave dsf
```


# pyenv installation and setup for Ubuntu

Tested with 20.04, which used Python 3.10.12 as default on 2024-05-13.

According to:

- <https://python.land/virtual-environments/pyenv>
- <https://github.com/pyenv/pyenv>
- <https://github.com/pyenv/pyenv-virtualenv>

## Purpose

pyenv:
- Use different Python versions for different projects
- Automatically switch Python version according to current directory (verify with `python3 --version`)

pyenv-virtualenv plugin:
- Managing multiple virtual envs
- Automatic activation/deactivation of virtual environments

## Install dependencies

```
sudo apt install libbz2-dev libncurses5-dev libffi-dev libreadline-dev libssl-dev libsqlite3-dev tk-dev liblzma-dev
```

## Installation on bash


```bash
curl https://pyenv.run | bash

# add to ~/.bashrc
echo 'export PYENV_ROOT="$HOME/.pyenv"' >> ~/.bashrc
echo 'command -v pyenv >/dev/null || export PATH="$PYENV_ROOT/bin:$PATH"' >> ~/.bashrc
echo 'eval "$(pyenv init -)"' >> ~/.bashrc

# add to ~/.profile

echo 'export PYENV_ROOT="$HOME/.pyenv"' >> ~/.profile
echo 'command -v pyenv >/dev/null || export PATH="$PYENV_ROOT/bin:$PATH"' >> ~/.profile
echo 'eval "$(pyenv init -)"' >> ~/.profile

exec "$SHELL"
```


## Installation on fish


```fish
set -Ux PYENV_ROOT $HOME/.pyenv
fish_add_path $PYENV_ROOT/bin

echo 'pyenv init - | source' >> ~/.config/fish/config.fish
echo 'status --is-interactive; and pyenv virtualenv-init - | source' >> ~/.config/fish/config.fish

exec "$SHELL"
```


## Setting versions

Update list of available versions:

```bash
pyenv update
```

List available versions:

```bash
pyenv install --list
```

Install desired version(s):

```
pyenv install 3.12     # latest version of 3.12
pyenv install 3.10.12  # what ubuntu 22.04 uses (not necessary?)
```

Set the default version to be used:

```bash
pyenv global system
```

Set local version to be used (for a specific directory)

```bash
cd my-project
pyenv local 3.12
```

check local version to be used

```bash
cd my-project
pyenv version
```

## Show activated venv on fish prompt

Create a file `~/.config/fish/functions/fish_prompt.fish`:

```fish
# Original author: Lily Ballard
# Extended by: Lukas Toggenburger

function fish_prompt --description 'Write out the prompt'
    set -l last_pipestatus $pipestatus
    set -lx __fish_last_status $status # Export for __fish_print_pipestatus.
    set -l normal (set_color normal)
    set -q fish_color_status
    or set -g fish_color_status red

    # Color the prompt differently when we're root
    set -l color_cwd $fish_color_cwd
    set -l suffix '>'
    if functions -q fish_is_root_user; and fish_is_root_user
        if set -q fish_color_cwd_root
            set color_cwd $fish_color_cwd_root
        end
        set suffix '#'
    end

    # Write pipestatus
    # If the status was carried over (if no command is issued or if `set` leaves the status untouched), don't bold it.
    set -l bold_flag --bold
    set -q __fish_prompt_status_generation; or set -g __fish_prompt_status_generation $status_generation
    if test $__fish_prompt_status_generation = $status_generation
        set bold_flag
    end
    set __fish_prompt_status_generation $status_generation
    set -l status_color (set_color $fish_color_status)
    set -l statusb_color (set_color $bold_flag $fish_color_status)
    set -l prompt_status (__fish_print_pipestatus "[" "]" "|" "$status_color" "$statusb_color" $last_pipestatus)

    #### ---------- adjustments for showing venv start here
    set -l venv (/home/tol/.pyenv/bin/pyenv version-name)
    set -l prompt_venv "never" # need to do this due to scoping
    if test "$venv" != "system"
        set prompt_venv "($venv) "
    else
        set prompt_venv ""
    end

    echo -n -s "$prompt_venv" (prompt_login)' ' (set_color $color_cwd) (prompt_pwd) $normal (fish_vcs_prompt) $normal " "$prompt_status $suffix " "
    #### ---------- adjustments for showing venv end here
end

```

This is based on `/usr/share/fish/tools/web_config/sample_prompts/default.fish`.

## Creating a venv in combination with pyenv

```bash
mkdir my-project
cd my-project

# optionally update list of available versions
pyenv update

# optionally install new version
pyenv install 3.13

# create venv
pyenv virtualenv 3.12 my_project_3.12

# set up project directory to use (and activate) the venv
pyenv local my_project_3.12

# show activated python version/venv
pyenv version
python3 --version

# show all python versions/venvs
pyenv versions

# remove venv at end of project
pyenv virtualenv-delete my_project_3.12

# remove python version (or venv)
pyenv uninstall 3.12
```


## Recreating a venv

```bash
# optional: list venvs
pyenv virtualenvs

# delete venv
pyenv virtualenv-delete my_project_3.12

# create venv
pyenv virtualenv 3.12 my_project_3.12
```


## Setup combination with poetry

bash:

```bash
poetry env use $(pyenv which python)
```

fish:

```fish
poetry env use (pyenv which python)
```

```
poetry install
```

And then each time:

```
poetry shell # spawns a new shell
# do some work and then
exit
```

or alternatively

```
poetry run python myscript.py
poetry run pytest
```

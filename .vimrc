set mouse=
set number
set scrolloff=7
set autoread
set incsearch
set hlsearch
"set smartindent
set autoindent
syntax on

" display vertical line to warn about long lines                                
highlight ColorColumn ctermbg=gray 
set cc=80
" Copy mode
command! C :set cc=0 nonumber
" IDE mode
command! I :set cc=80 number

" Search mappings: These will make it so that going to the next one in a
" search will center on the line it's found in.
map N Nzz
map n nzz

" fix markdown syntax highlighting on Ubuntu 18.04/18.10
" set term=screen-256color

set foldmethod=syntax
set foldlevel=99
let xml_syntax_folding=1

" force markdown for .md files ( https://github.com/tpope/vim-markdown )
autocmd BufNewFile,BufReadPost *.md set filetype=markdown

" don't hide long lines by showing @ characters ( http://stackoverflow.com/questions/21955785/why-does-vim-hide-wrapped-lines-and-prints-multiple-s )
:set display+=lastline

" auto-reload vim configs when they're changed (cross-platform), from http://stackoverflow.com/a/2403926
augroup myvimrc
	au!
	au BufWritePost .vimrc,_vimrc,vimrc,.gvimrc,_gvimrc,gvimrc so $MYVIMRC | if has('gui_running') | so $MYGVIMRC | endif
augroup END

" for python files: tab = 4 spaces ( http://stackoverflow.com/a/9986497 )
autocmd Filetype python setlocal expandtab tabstop=4 shiftwidth=4

autocmd Filetype rust setlocal expandtab tabstop=4 shiftwidth=4

" User defined commands for adding a terminal in the
" right (:Vter) or bottom (:Hter) half
command Vter :vertical :botright :term
command Hter :botright :term

" Open new split panes to right and bottom                                      
set splitbelow                                                                  
set splitright 

" Allow pasting something multiple times, from https://stackoverflow.com/questions/3837772/vim-replace-selection-with-default-buffer-without-overwriting-the-buffer#comment76964481_3837845
vnoremap p "_dP

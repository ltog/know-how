# JetBrains

## Using vim mode

### Where to put your config

Use `~/.ideavimrc`

### What to put there

```
set scrolloff=7
set incsearch
set hlsearch

" disable audible bell, https://intellij-support.jetbrains.com/hc/en-us/community/posts/205806309-How-to-disable-completely-audible-beeps-
set visualbell

" Search mappings: These will make it so that going to the next one in a
" search will center on the line it's found in.
map N Nzz
map n nzz
                                                                                
" https://github.com/JetBrains/ideavim/blob/master/doc/ideajoin-examples.md     
set ideajoin     
```

### Force reloading .ideavimrc

In your JetBrains IDE type:

    :source ~/.ideavimrc

## Activate soft-wrap for Markdown

File -> Settings -> Editor -> General -> Soft Wraps -> [X] Soft-wrap these files

# Infrastructure

## Installation

```bash
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
```

According to <https://www.rust-lang.org/tools/install>

### Add support for fish

```fish
set -U fish_user_paths $HOME/.cargo/bin $fish_user_paths
```

from https://stackoverflow.com/a/34401308

TODO: How to undo this?

## Updating Rust

```bash
rustup update
```

## Uninstallation

```bash
rustup self uninstall
```

## Reading the docs

```bash
rustup doc
```

## Compiling and running a first program (without cargo)

Create a new file `hello.rs`:

```rust
fn main() {
    // Statements here are executed when the compiled binary is called
    println!("Hello World!");
}
```

Compile it:

```bash
rustc hello.rs
```

Run it:

```bash
./hello
```

## Compiling and running a first program (with cargo)

Create a new project:

```bash
cargo new hello
cd hello
```

This will create a file `src/main.rs`.

Compile and run it:

```bash
cargo run
```

which is short for:

```bash
cargo build
./target/debug/hello
```

## Checking (sort of compiling, but not building an executable)

```bash
cargo check
```

# Basic language concepts

## Comments

```rust
// this is a (line) comment, used to document code flow and/or internals

//! this is an inner line doc comment
//! this is used at the start of a file to document modules and crates

/// this is a outer line documentation comment (used on types, traits, functions, ...)
/// use it to generate HTML documentation and define software tests
/// supports Markdown
/// commonly used sections are:
/// - `# Examples`
/// - `# Panics`
/// - `# Errors`
/// - `# Safety`
```

## Variables and assignments

Variables:

```rust
let mut x = 5;
x = 6;
```

- `mut` needs to be present for overwriting a previous value

Constants:

```rust
const MAX_POINTS: u32 = 100_00;
```

- It is not possible to use `mut`
- The type of value must be annotated
- Constants can be declared in any scope, including global scope
- May only be set to values known at compile-time

Shadowing:

```rust
let x = 1;
let x = 2*x;
```

- Variable can be immutable after last `let`
- Variable name can be reused while type of variable may change

Integer types:

| Length in bits | Signed          | Unsigned |
|----------------|-----------------|----------|
| 8              | `i8`            | `u8`     |
| 16             | `i16`           | `u16`    |
| 32             | `i32` (default) | `u32`    |
| 64             | `i64`           | `u64`    |
| 128            | `i128`          | `u128`   |
| arch           | `isize`         | `usize`  |

Note: The primary situation in which you’d use `isize` or `usize` is when indexing some sort of collection.


Integer literals:

| Number literals | Example       |
|-----------------|---------------|
| Decimal         | `98_222`      |
| Hex             | `0xff`        |
| Octal           | `0o777`       |
| Binary          | `0b1111_0000` |
| Byte (u8 only)  | `b'A'`        |


Booleans:

```rust
let t = true;
let f: bool = false;
```

Note: Prefer `enum E { Visible, Hidden }` over `visible: bool`

## Generating output

### Write to stdout/stderr

`print!`/`println! macro`:

```
let a = 1;
println!("a is {}", a);
println!("a is {a}");   # (since Rust 1.58, Jan 2022) (using "captured identifiers in format strings", similar to Python's f-strings)
println!("a = {a:?}");  # as above
println!("a = {a:#?}"); # as above
```

`eprint!`/`eprintln! macro`:

- like `print!`
- writes to stdout

### String formatter

- `{}`:
  - output used for end user consumption
  - needs trait `std::fmt::Display`
- `{:?}`:
  - single-line output for developers
  - needs trait `Debug`:
    - `#[derive(Debug)]`
- `{:#?}`:
  - multi-line output (aka pretty-print) for developers
  - needs trait `Debug`
    - `#[derive(Debug)]`

### `dbg!` macro

- Prints to stderr
- Output is similar to `{:#?}`
- returns value, so you can write `let a = dbg!(3 * 2)`;


## Strings etc.

### The different data types

The most simple type, a **character literal (char)**, defined by _single quotes_:

```rust
let ferris = '🦀'; // fixed 4 byte unicode 'char'
```

- stored on stack

A **string literal**, defined by _double quotes_:

```rust
let mut ferrises = "🦀🦀🦀"; // mut is ok, but the literal itself is immutable
```

or with explicit type:

```rust
let mut ferrises: &str = "🦀🦀🦀";
```


- stored on stack

The **String** type:

```rust
let mut s1 = String::new();
let mut s2 = String::from("hello");
s2.push_str(", world!"); // s is mutable
```

The **string slice**:

```rust
let ferrises = "🦀🦀🦀";
let ferris = &ferrises[0..4]; // emoji is 4 bytes in UTF-8
println!("ferris = {}", ferris);

let interrobangs = "‽‽‽";
let interrobang = &interrobangs[0..3]; // interrobang is 3 bytes in UTF-8
println!("interrobang = {}", interrobang);
```

### Conversions

String literal to String:

```rust
let data = "initial contents";
let s = data.to_string();

// or directly
let s = "initial contents".to_string();

// or equivalent
let s = String::from("initial contents");
```

### Usage in function parameters

Instead of using String references in a function signature:

```rust
fn first_word(s: &String) -> &str {
```

one should use string slices:

```rust
fn first_word(s: &str) -> &str {
```

This makes an API more general without loss of functionality.

Calls to this API then would look like this:

```rust
fn main() {
    let my_string = String::from("hello world");

    // `first_word` works on slices of `String`s, whether partial or whole
    let word = first_word(&my_string[0..6]);
    let word = first_word(&my_string[..]);
    // `first_word` also works on references to `String`s, which is equivalent
    // to a slice of the whole `String`
    let word = first_word(&my_string);

    let my_string_literal = "hello world";

    // `first_word` works on slices of string literals, whether partial or whole
    let word = first_word(&my_string_literal[0..6]);
    let word = first_word(&my_string_literal[..]);

    // Because string literals *are* string slices already,
    // this works too, without the slice syntax!
    let word = first_word(my_string_literal);
}
```


### Concatenate Strings

**+ Operator** (only for `String`):

```rust
let mut a = String::from("asdf");
let b = String::from("jklö");
a += &b;
```

**Variants of pushing**:

```rust
let mut a = String::from("asdf");
a.push('j');
a.push_str("kl");
a.push_str(String::from("ö"));
```

**format! macro**:

```rust
let combination = format!("{}{}", &s, &s); // returns String
```

**Array.concat()**:

Example:

```rust
let combination = ["hi", " ", "there"].concat(); // returns String
```

|                | char | string literal | String | string slice |
|----------------|------|----------------|--------|--------------|
| char           | ❌   | ❌             | ❌     | ❌           |
| string literal | ❌   | ✅             | ✅     | ✅           |
| String         | ❌   | ✅             | ✅ †   | ✅           |
| string slice   | ❌   | ✅             | ✅     | ✅           |

† The following code does not work:

```rust
let combination = [&my_string, &my_string].concat();
```

In order to make this work, we need one of the following:

Either give up ownership of the strings:

```rust
let combination = [my_string.clone(), my_string].concat();
```

or make sure, that at least one of the arguments is of type `str`:

```rust
// Variant 1
let combination = [&my_string, "", &my_string].concat();

// Variant 2
let combination = [&my_string, my_string.as_str()].concat();

// Variant 3
let combination = [&my_string, &*my_string].concat();

// Variant 4
let combination = ([&my_string, &my_string] as [&str; 2]).concat();
```

Read more about concatenating strings in general here: <https://maxuuell.com/blog/how-to-concatenate-strings-in-rust>

### Iterate over Strings

Iterating over Unicode scalar values (note that this text contains diacritics):

```rust
for c in "नमस्ते".chars() {
    println!("{}", c);
}
```

Iterating over raw bytes:

```rust
for b in "नमस्ते".bytes() {
    println!("{}", b);
}
```

Iterating over grapheme clusters: See crates on <https://crates.io/>.

## Compound types

Compound types group multiple values into one type. There are:

- Tuples
  - Tuple structs
  - Unit type
- Arrays
- Structs

### Tuples

Properties:

- No named elements
- No type safety when doing assignments, etc.
- Data types may be mixed
- Fixed length

```rust
fn main() {
    let tup: (i32, f64, u8) = (500, 6.4, 1); // optionally skip types

    // Access elements using _destructuring_
    let (x, y, z) = tup;
    println!("The value of y is: {}", y);

    // Access elements using period
    let six_point_four = x.1;
}
```

Special case: Tuple structs:

- No named elements
- Type safety when doing assignments, etc.

```rust
struct Color(i32, i32, i32);
struct Point(i32, i32, i32);

let black = Color(0, 0, 0);
let origin = Point(0, 0, 0);
```

Special case: Unit type:

```rust
struct AlwaysEqual;

let subject = AlwaysEqual;
```

### Arrays

Properties:

- Data types must be the same
- Fixed length (like tuples)
- Allocates data on stack

```rust
let months: [&str; 12] = ["January", "February", "March", "April", "May", "June", "July",
                          "August", "September", "October", "November", "December"];

// convert string slices to String
let months = months.iter().map(|x| x.to_string()).collect::<Vec<_>>();

let slice = &months[0..2]; // array slicing (uses indices 0, 1, NOT 2!)

let zeroes = [0; 10]; // fill with 10 zeroes

let first = zeroes[0]; // access first element
```

Iterate over array:

```rust
let a = [10, 20, 30, 40, 50];
for element in a.iter() {
    println!("the value is: {}", element);
}
```

Iterate over array with access to index:

```rust
fn main() {
    let a = [10, 20, 30, 40, 50];
    for (index, item) in a.iter().enumerate() {
        println!("at index {} the value is: {}", index, item);
    }
}
```

### Structs

Properties:

- Data types may be mixed

Defining a struct:

```rust
struct User {
    username: String,
    email: String,
    sign_in_count: u64,
    active: bool,
}
```

Creating a struct instance:

```rust
fn build_user(email: String, username: String) -> User {
    User {
        email: email,
        // field init shorthand syntax:
        // function parameter and struct field have the same name
        username,
        active: true,
        sign_in_count: 1,
    }
}

fn main() {
    let user1 = build_user(
        String::from("someone@example.com"),
        String::from("someusername123"),
    );
}

```

Accessing a struct value:

```rust
let myusername = user1.username.clone();
```

Copying a struct instance:

```rust
let user2 = user1;

let user3 = User { // this moves data (strings email, username),
                   // i.e. user2 won't be usable afterwards
        active: false,
        ..user2    // <-- import part is here
}
```

A struct using generic values:

```rust
struct Point<T> {
    x: T,
    y: T,
}

fn main() {
    let integer = Point { x: 5, y: 10 };
    let float = Point { x: 1.0, y: 4.0 };
}
```

## Enums

Examples:

```rust
enum IpAddrKind {
    V4,
    V6,
}

let four = IpAddrKind::V4; // do not use parentheses here
```

```rust
enum IpAddr {
    V4(u8, u8, u8, u8),
    V6(String),
}

// using automatically generated constructor functions for getting enum instances
let home = IpAddr::V4(127, 0, 0, 1);
let loopback = IpAddr::V6(String::from("::1"));
```

An enum with variants that store different types and checking for a $pecific variant using `if let`:

```rust
enum Message {
    Quit,                       // literal value, no associated data
    Move { x: i32, y: i32 },    // named fields, like a struct
    Write(String),              // a single String
    ChangeColor(i32, i32, i32), // three i32 values
}

fn main() {
    let msg = Message::Write(String::from("asdf"));
    if let Message::Write(blub) = msg {
        println!("msg is a Write with content = {}", blub);
    }
}
```

## Functions

Function bodies contain statements, optionally ending in an expression:

```rust
fn some_function() -> i32 {
    let a = 1; // statement
    let b = 2; // statement
    let c = 3; // statement

    c + 1 // expression, will serve as return value of this function
          // note the absence of a semicolon!
}
```

## Associated functions (`impl`)

Associated functions (inside `impl`) are similar to functions (`fn`) but defined within the context of a struct, enum or trait object.

### Methods

Methods always have `self` as their first parameter.

Example:

```rust
#[derive(Debug)]
struct Rectangle {
    width: u32,
    height: u32,
}

impl Rectangle {
    fn area(&self) -> u32 { // also possible: `&mut self` or `self` (rare)
                            // `&self` is short for `self: &Self`
        self.width * self.height
    }

    fn can_hold(&self, other: &Rectangle) -> bool {
        self.width > other.width && self.height > other.height
    }
}

fn main() {
    let rect1 = Rectangle {
        width: 30,
        height: 50,
    };

    println!(
        "The area of the rectangle is {} square pixels.",
        rect1.area()
    );

    println!("The rectangle's width is {} pixels.", rect1.width);
}
```

### Functions without `self` parameter

Functions inside `impl` block, but without `self` parameter can be used e.g. for constructors:

```rust
impl Rectangle {
    fn square(size: u32) -> Rectangle {
        Rectangle {
            width: size,
            height: size,
        }
    }
}

let sq = Rectangle::square(3);
```


## Control structures

### `match`

Properties:

- Does not drop temporary values until the end of the associated block (might be relevent e.g. for multi-threaded programs)

```rust
#[derive(Debug)] // so we can inspect the state in a minute
enum UsState {
    Alabama,
    Alaska,
    // --snip--
}

enum Coin {
    Penny,
    Nickel,
    Dime,
    Quarter(UsState), // quarters have 50 different state designs
}

fn value_in_cents(coin: Coin) -> u8 {
    match coin {
        Coin::Penny => 1,
        Coin::Nickel => 5,
        Coin::Dime => 10,
        Coin::Quarter(state) => {
            println!("State quarter from {:?}!", state);
            25
        }
    }
}

fn main() {
    value_in_cents(Coin::Quarter(UsState::Alaska));
}
```

Handle all currently unhandled cases in a `match` arm by putting these at the end:

- `otherr => some_func(otherr);` : Do something with value (can be any name)
- `_ = > some_func();` : Do something without using value
- `_ = ();` : Do nothing (uses the _unit value_)


```rust
use std::cmp::Ordering;

match a.cmp(&b) { // this match expression has 3 arms
    Ordering::Less => println!("a < b"),
    Ordering::Greater => { // use { } for multiple statements
        println!("a > b");
        println!("a > b");
    }
    Ordering::Equal => println("a == b"), // the last comma is optional
}
```

### Infinite loop

```rust
loop {
    // do something
    break; // eventually quit loop
}
```

### Loop with return value

```rust
let result = loop {
    break 5;
}
```

### if/else if/else

```rust
if a % 4 == 0 {
    ...
} else if a % 3 == 0 {
    ...
} else {
    ...
}
```

### if/else with assignment

```rust
let number = if condition {
    11
} else {
    22
};
```

### while loop

```rust
fn main() {
    let mut number = 3;
    while number != 0 {
        println!("{}!", number);
        number -= 1;
    }
    println!("LIFTOFF!!!");
}
```

## Console I/O

Write string to console:

```rust
println!("Hello, world!");
```

Read string from console:

```rust
use std::io;

let mut input = String::new();
io::stdin().read_line(&mut input)
    .expect("Could not read line.");
println!("You typed: {}", input);
```

Read integer from console:

```rust
use std::io;

let mut input = String::new();

io::stdin().read_line(&mut input)
    .expect("Could not read line.");

let input: u32 = input.trim().parse() // shadowing: reuse variable `input`
    .expect("Could not convert string to number.");

// alternative with better error handling

let mut input: u32 = input.trim().parse() {
    Ok(num) => num,
    Err(_) => handle_error_somehow(),
};
```



## Important enums

### `Option`

Type `Option` represents an optional value: every `Option` is either `Some` and contains a value, or `None`, and does not.

```rust
pub enum Option<T> {
    None,
    Some(T),
}
```

The `?` (question mark) operator can be used in functions that return `Option`.

Examples:

Assign Some/None:

```rust
let some_string = Some("a string");    // type inference works
let absent_number: Option<i32> = None; // type needs to be specified
```

Evaluate `Option` using `match`:

```rust
fn plus_one(x: Option<i32>) -> Option<i32> {
    match x {
        None => None,
        Some(i) => Some(i + 1),
    }
}

let five = Some(5);
let six = plus_one(five);
let none = plus_one(None);
```

from: https://doc.rust-lang.org/stable/book/ch06-03-if-let.html

Evaluate `Option` using `if let`:

```rust
let the_val = Some(5);
//let the_val: Option<i32> = None; // alternative to above line
if let Some(my_val) = the_val { // read: if `let` destructures `the_val` into `Some(my_val)`, evaluate { }
    println!("The value is {}", my_val);
} // optionally add `else { }`
```

from https://doc.rust-lang.org/stable/book/ch06-03-if-let.html


Links:

- https://doc.rust-lang.org/std/option/enum.Option.html


### `Result`

`Result<T, E>` is the type used for returning and propagating errors.

```rust
enum Result<T, E> {
   Ok(T),
   Err(E),
}
```
Following, some variants on how to handle `Result` enums:

Variant 1: `match`:

```rust
use std::fs::File;

fn main() {
    let f = File::open("hello.txt");

    let f = match f {
        Ok(file) => file,
        Err(error) => panic!("Problem opening the file: {:?}", error),
    };
}
```

Variant 2: `unwrap()` (just use it or panic)

```rust
use std::fs::File;

fn main() {
    let f = File::open("hello.txt").unwrap();
}
```

Variant 3: `expect()` (similar to `unwrap`, but with custom error message):

```rust
use std::fs::File;

fn main() {
    let f = File::open("hello.txt").expect("Failed to open hello.txt");
}
```

Variant 4a: Propagate error using `?` The `?` (question mark) operator can be used in functions that return `Result`. If possible, `T` from `Ok` will be unpacked and the program continues, otherwise the function immediately ends, returning the error.

```rust
use std::fs::File;

fn read_username_from_file() -> Result<String, io::Error> {
    let mut f = File::open("hello.txt")?;
    // do something with f
}
```

In order for this to work, the error values need to implement the `From` trait.

Variant 4b: Using multiple `?`:

```rust
#![allow(unused)]
fn main() {
    use std::fs::File;
    use std::io;
    use std::io::Read;

    fn read_username_from_file() -> Result<String, io::Error> {
        let mut s = String::new();

        File::open("hello.txt")?.read_to_string(&mut s)?;

        Ok(s)
    }
}
```

Variant 5: Using convencience function:

```rust
#![allow(unused)]
fn main() {
    use std::fs;
    use std::io;

    fn read_username_from_file() -> Result<String, io::Error> {
        fs::read_to_string("hello.txt")
    }
}
```


Links:

- https://doc.rust-lang.org/book/ch09-02-recoverable-errors-with-result.html#recoverable-errors-with-result
- https://doc.rust-lang.org/std/result/index.html

## Standard collections

### Vector

Properties:

- stored on heap
- index starts at 0
- Elements can NOT be added, while holding a reference to one of the elements.

Operations:

- Create:
  - `let mut v: Vec<i32> = Vec::new();`
  - `let mut v = vec![1, 2, 3];` (shorter)
- Get number of elements:
  - `let l = v.len()`
  - special case: `v.is_empty()`
- Read element:
  - `let third: &i32 = &v[2];` (gives reference, panics for non-existing element)
  - `v.get(2)` (gives `Option<&T>`, can be processed using `match`)
- Iterate:
  - Generally: `for` returns reference
  - Read-only:
    - v as value:
      - `for i in &v { println!("{}", *i); }`
      - `for &i in &v { println!("{}", i); }`
      - `for (index, value) in v.iter().enumerate() {println!("{} is at index {}", value, index);}`
    - v as reference (e.g. `&[T]`):
      - `for i in v { println!("{}", *i); }`
      - `for &i in v { println!("{}", i); }`
  - Writing:
    - `for i in &mut v { *i += 1; }`
- Add item:
  - `v.push(42);`
- Remove item:
  - `let popped = v.pop()`
- Compare to other (returns `bool`):
  - `v1.iter().eq(v2.iter())`
- Print:
  - `let v2 = vec![1; 10]; println!("{v2:?}");` (since Rust 1.58, Jan 2022) (using "captured identifiers in format strings")

Pattern "Storing enum values in a vector":

```rust
enum SpreadsheetCell {
    Int(i32),
    Float(f64),
    Text(String),
}

let row = vec![
    SpreadsheetCell::Int(3),
    SpreadsheetCell::Text(String::from("blue")),
    SpreadsheetCell::Float(10.12),
];
```

from: https://doc.rust-lang.org/book/ch08-01-vectors.html

#### Consider allowing slices

If you are tempted to write a function signature like this

```rust
fn mean(nums: &Vec<i32>) -> f64 { ... }
```

consider rewriting it to

```rust
fn mean(nums: &[i32]) -> f64 { ... }
```

thereby allowing slices to be used as an argument. The function body possibly work without being changed and existing function calls don't have to be adjusted:

```rust
let mean_val = mean(&nums); // ok for both variants
let mean_val_slice = mean(&nums[2..4]); // new call that uses slices
```


### HashMap

- Create:
  - Basic: `use std::collections::HashMap; let mut hashmap = HashMap::new();`
  - From vectors of keys and values: `use std::collections::HashMap; let teams = vec![String::from("Blue"), String::from("Yellow")]; let initial_scores = vec![10, 50]; let mut scores: HashMap<_, _> = teams.into_iter().zip(initial_scores.into_iter()).collect(); `
- Read element:
  - `let value = hashmap.get(&key)` (returns `Option<&V>`)
  - `let value = &hashmap[&key]` (dangerous, panics if key does not exist)
- Iterate:
  - Read-only: `for (key, value) in &hashmap { println!("{}: {}", key, value); }`
  - Writing: `for word in text.split_whitespace() { let count = map.entry(word).or_insert(0); *count += 1; }`
- Iterate (sorted): (needs `use itertools::Itertools;`)
  - By key, read-only: `for (key, value) in hashmap.iter().sorted_by_key(|x| x.0)`
  - By value, read-only: `for (key, value) in hashmap.iter().sorted_by_key(|x| x.1)`
  - Writing: use `iter_mut()` instead of `iter()` ([only for values, not for keys](https://stackoverflow.com/questions/65580524/how-to-update-a-key-in-a-hashmap), [sorting by value and changing them at the same time not easily possible](https://stackoverflow.com/a/70125221/2422702))
  - Reverse order: Append `.rev()`
- Add item:
  - Overwrite value if existing: `hashmap.insert("String::from("Blue"), 10);`
  - Only insert value if non-existing: `hashmap.entry("String::from("Blue")).or_insert(10);`
- Remove item: TODO
- Is key contained?
  - `hashmap.contains_key(k)` (returns `bool`)

## Ownership & Borrowing

Ownership rules:

- Each value in Rust has a variable that is called its _owner_.
- There can only be one owner at a time.
- When the owner goes out of scope, the value will be dropped.
- If a type (stored on stack) implements the `Copy` trait, an older variable is still usable after assignment/function call.
- Rust won’t let us annotate a type with the `Copy` trait if the type, or any of its parts, has implemented the `Drop`` trait.
- At any given time, you can have either one mutable reference or any number of immutable references

Example with data on stack:

```rust
let x = 5; // bind value of 5 to x
let y = x; // make a copy of value in x (onto stack) and bind it to y
```

Example with String (on heap):

```rust
let s1 = String::from("hello");

let s2 = s1; // copy ptr, len, capacity; do NOT copy string itself
             // invalidate s1, you can't access it anymore from here on
             // invalidation happens since String does not implement Copy trait

let s3 = s2.clone(); // deep copy
```

Calling a function _moves_ a variable (if it does not implement the `Copy` trait):

This does not work due to _move_:

```rust
fn main() {
    let s = String::from("hello");
    let l = str_len(s); // moves s into function str_len
    println!("Length of {} is {}", s, l); // s is not accessible anymore
}

fn str_len(s: String) -> usize {
    s.len()
}
```

We can fix it by using _references_. Having references as function parameters is called _borrowing_.

```rust
fn main() {
    let s = String::from("hello");
    let l = str_len(&s); // moves s into function str_len
    println!("Length of {} is {}", s, l); // s is not accessible anymore
}

fn str_len(s: &String) -> usize {
    s.len()
}
```

Then there are mutable references, allowing to write to borrowed values:

```rust
fn main() {
    let mut s = String::from("hello");
    change(&mut s);
}

fn change(some_string: &mut String) {
    some_string.push_str(", world");
}
```

But you can't have more than one mutable reference at one point in time:

```rust
let mut s = String::from("hello");

let r1 = &mut s;
let r2 = &mut s;

println!("{}, {}", r1, r2);
```

Interestingly, this is ok:

```rust
fn main() {
    let mut s = String::from("hello");

    let r1 = &mut s;
    println!("{}", r1);
    // the reference's scope ends here, since it is not used anymore

    let r2 = &mut s;
    println!("{}", r2);
}
```

# Practical work

## Measuring execution times

```rust
use std::time::Instant;

let before = Instant::now();
workload();
println!("Elapsed time: {:.2?}", before.elapsed());
```

from <https://stackoverflow.com/a/57341631/2422702>

## Exit with error code

```rust
use std::process;
eprintln!("No args given");
process::exit(1);
```

## Read arguments

### Using a library

See <https://crates.io/crates/clap/versions> (which as of version 3 seems to obsolete <https://crates.io/crates/structopt>)

### By hand

Short example:

```rust
use std::env;
let args: Vec<String> = env::args().collect();
for arg in &args[1..] {
    do_something(arg);
}
```

Full example (basically <https://doc.rust-lang.org/stable/book/ch12-03-improving-error-handling-and-modularity.html#calling-confignew-and-handling-errors>):

```rust
use std::env;
use std::process;

fn main() {
    let args: Vec<String> = env::args().collect();

    let config = Config::new(&args).unwrap_or_else(|err| {
        eprintln!("Problem parsing arguments: {}", err);
        process::exit(1);
    });

    todo!();
}

struct Config {
    query: String,
    filename: String,
}

impl Config {
    fn new(args: &[String]) -> Result<Config, &str> {
        if args.len() < 3 {
            return Err("not enough arguments");
        }

        let query = args[1].clone();
        let filename = args[2].clone();

        Ok(Config { query, filename })
    }
}

```

## Sleep for a given time

```rust
use std::{thread, time::Duration};

fn main() {
    thread::sleep(Duration::from_millis(4000));
}
```

# Error handling

`unwrap()`

```rust
use std::fs::File;

fn main() {
    let f = File::open("hello.txt").unwrap();
}
```

# Generics

## Generics when using structs

Example with same type coordinates:

```rust
struct Point<T> {
    x: T,
    y: T,
}

fn main() {
    let integer = Point { x: 5, y: 10 };
    let float = Point { x: 1.0, y: 4.0 };
}

impl Point<f32> {
    fn distance_from_origin(&self) -> f32 {
        (self.x.powi(2) + self.y.powi(2)).sqrt()
    }
}
```

Example with mixed type coordinates:

```rust
struct Point<T, U> {
    x: T,
    y: U,
}

impl<T, U> Point<T, U> {
    fn mixup<V, W>(self, other: Point<V, W>) -> Point<T, W> {
        Point {
            x: self.x,
            y: other.y,
        }
    }
}

fn main() {
    let both_integer = Point { x: 5, y: 10 };
    let both_float = Point { x: 1.0, y: 4.0 };
    let integer_and_float = Point { x: 5, y: 4.0 };

    let p1 = Point { x: 5, y: 10.4 };
    let p2 = Point { x: "Hello", y: 'c' };
    let p3 = p1.mixup(p2);
}
```

## Default generic type parameters

```rust
use std::ops::Add;

struct Millimeters(u32);
struct Meters(u32);

impl Add<Meters> for Millimeters {
    type Output = Millimeters;

    fn add(self, other: Meters) -> Millimeters {
        Millimeters(self.0 + (other.0 * 1000))
    }
}
```

# Traits

## Basic usage of traits

Properties:

- We can implement a trait on a type only if either the trait or the type is local to our crate. (This is due to a property called _coherence_.)

Example:

```rust
pub trait Summary { // pub: another crate can implement this trait
    fn summarize(&self) -> String;
}

// default implementation for trait
pub trait Summary2 {
    fn summarize(&self) -> String {
        String::from("(Read more...)")
    }
}

pub struct NewsArticle {
    pub headline: String,
    pub location: String,
    pub author: String,
    pub content: String,
}

// define implementation of Summary trait for NewsArticle
impl Summary for NewsArticle {
    fn summarize(&self) -> String {
        format!("{}, by {} ({})", self.headline, self.author, self.location)
    }
}

// instruct NewsArticle to use default implementation
impl Summary2 for NewsArticle {}

pub struct Tweet {
    pub username: String,
    pub content: String,
    pub reply: bool,
    pub retweet: bool,
}

impl Summary for Tweet {
    fn summarize(&self) -> String {
        format!("{}: {}", self.username, self.content)
    }
}

fn main() {
    let tweet = Tweet {
        username: String::from("horse_ebooks"),
        content: String::from(
            "of course, as you probably already know, people",
        ),
        reply: false,
        retweet: false,
    };

    println!("1 new tweet: {}", tweet.summarize());
}
```

## Specifying traits as function parameters

`impl Trait` syntax:

```rust
pub fn notify(item: &impl Summary) {
    println!("Breaking news! {}", item.summarize());
}
```

_trait bound_ syntax:

```rust
pub fn notify<T: Summary>(item: &T) {
    println!("Breaking news! {}", item.summarize());
}
```

Use two arguments (of any type):

```rust
pub fn notify(item1: &impl Summary, item2: &impl Summary) {
```

Use two arguments (that must be the same type):

```rust
pub fn notify<T: Summary>(item1: &T, item2: &T) {
```

Use multiple trait bounds (using `+` syntax):

```rust
pub fn notify(item: &(impl Summary + Display)) {
```

```rust
pub fn notify<T: Summary + Display>(item: &T) {
```

```rust
fn largest<T: PartialOrd + Copy>(list: &[T]) -> T {
    let mut largest = list[0];

    for &item in list {
        if item > largest {
            largest = item;
        }
    }

    largest
}
```

### Using `where` clauses

Without `where`:

```rust
fn some_function<T: Display + Clone, U: Clone + Debug>(t: &T, u: &U) -> i32 {
```

With `where`:

```rust
fn some_function<T, U>(t: &T, u: &U) -> i32
    where T: Display + Clone,
          U: Clone + Debug
{
```

## Specifying traits as return types

```rust
fn returns_summarizable() -> impl Summary {
    Tweet {
        ...
    }
}
```

## Using trait bounds to conditionally implement methods

```rust
use std::fmt::Display;

struct Pair<T> {
    x: T,
    y: T,
}

impl<T> Pair<T> {
    fn new(x: T, y: T) -> Self {
        Self { x, y }
    }
}

// this function is only implemented if T implements PartialOrd and Display traits
impl<T: Display + PartialOrd> Pair<T> {
    fn cmp_display(&self) {
        if self.x >= self.y {
            println!("The largest member is x = {}", self.x);
        } else {
            println!("The largest member is y = {}", self.y);
        }
    }
}
```

## Blanket trait implementation

The standard library implements the `ToString` trait on any type tht implements the `Display` trait:

```rust
impl<T: Display> ToString for T {
    // --snip--
}

// we can call `to_string()` ony any type that implements the Display trait
let s = 3.to_string();
```

## Trait objects

Properties:

- Limitation of traits that can be used as trait objects: Only so-called _object-safe traits_ can be used in trait objects. Traits are object-safe if they meet all following conditions:
  - The return type isn't `Self`
  - There are no generic type parameters

Example:

```rust
// Define trait `Draw` with one function `draw`
// all drawable strcuts/enums must implement this
pub trait Draw {
    fn draw(&self);
}

// ===== Variant 1 (using trait objects) to call draw() on all components
// + allows for elements of different types
// - slower


// the `components` field holds trait objects
// these trait objects have type `Box<dyn Draw>` or generally `Box<dyn SomeTrait>`
// `Box<dyn Draw>` means any type inside a Box that implements the `Draw` trait
pub struct Screen {
    pub components: Vec<Box<dyn Draw>>,
}

impl Screen {
    pub fn draw_all(&self) {
        for component in self.components.iter() {
            component.draw();
        }
    }
}

// ===== Variant 2 (using generic type) to call draw() on all components
// + will do monomorphization at compile time
// - all vector elements must be of same type

pub struct Screen<T: Draw> {
    pub components: Vec<T>, // all vector elements must be of same type
}

impl<T> Screen<T>
where
    T: Draw,
{
    pub fn draw_all(&self) {
        for component in self.components.iter() {
            component.draw();
        }
    }
}
```

Long example (basically listing 17-18 from chapter 17.3):

Advantages:

- Functionality for a state is defined in one place
- Context (here `Post`) that defines the user interface knows nothing about states
- Easy to extend

Disadvantages:

- Some states are coupled to each other: Adding a state between `PendingReview` and `Published` would require a change in `PendingReview`.
- Duplication of logic (`request_review` and `approve`) in multiple states: Default implementations in trait `State` that return `self` would be nice, but would violate object safety.
- Duplication of logic (`request_review` and `approve`) in `Post`.
- Not taking full advantage of Rust's strengths

```rust
fn main() {
    let mut post = Post::new();

    println!("Going to run: post.add_text()");
    post.add_text("I ate a salad for lunch today");
    assert_eq!("", post.content());

    println!("Going to run: post.request_review()");
    post.request_review();
    assert_eq!("", post.content());

    println!("Going to run: post.approve()");
    post.approve();
    assert_eq!("I ate a salad for lunch today", post.content());
}

pub struct Post {
    state: Option<Box<dyn State>>,
    content: String,
}

impl Post {
    pub fn new() -> Post {
        Post {
            state: Some(Box::new(Draft {})),
            content: String::new(),
        }
    }

    pub fn add_text(&mut self, text: &str) {
        self.content.push_str(text);
    }

    pub fn content(&self) -> &str {
        self.state.as_ref().unwrap().content(self)
    }

    pub fn request_review(&mut self) {
        if let Some(s) = self.state.take() {
            self.state = Some(s.request_review())
        }
    }

    pub fn approve(&mut self) {
        if let Some(s) = self.state.take() {
            self.state = Some(s.approve())
        }
    }

trait State {
    fn request_review(self: Box<Self>) -> Box<dyn State>;
    fn approve(self: Box<Self>) -> Box<dyn State>;

    fn content<'a>(&self, post: &'a Post) -> &'a str {
        ""
    }
}

struct Draft {}

impl State for Draft {
    fn request_review(self: Box<Self>) -> Box<dyn State> {
        Box::new(PendingReview {})
    }

    fn approve(self: Box<Self>) -> Box<dyn State> {
        self
    }
}

struct PendingReview {}

impl State for PendingReview {
    fn request_review(self: Box<Self>) -> Box<dyn State> {
        self
    }

    fn approve(self: Box<Self>) -> Box<dyn State> {
        Box::new(Published {})
    }
}

struct Published {}

impl State for Published {
    fn request_review(self: Box<Self>) -> Box<dyn State> {
        self
    }

    fn approve(self: Box<Self>) -> Box<dyn State> {
        self
    }

    fn content<'a>(&self, post: &'a Post) -> &'a str {
        &post.content
    }
}

```

Long example (basically listing 17-20 from chapter 17.3, similar to example before but more idiomatic):

Basic idea: Encode state into different types (structs), methods are only available as applicable for each type. Calling a function that leads to a state-transition will take ownership of the old instance and return a new instance (of a new type).

Advantages:

- No lingering instances of e.g. `DraftPost`
- Impossible to read `content` of `PendingReviewPost` using `content` method, since it does not exist
- Blog workflow is encoded in type system, checked at compile time

Disadvantages:

- No strict encapsulation


```rust
fn main() {
    let mut post = Post::new(); // type: DraftPost

    println!("Going to run: post.add_text()");
    post.add_text("I ate a salad for lunch today");

    println!("Going to run: post.request_review()");
    let post = post.request_review(); // type: PendingReviewPost

    println!("Going to run: post.approve()");
    let post = post.approve(); // type: Post
    assert_eq!("I ate a salad for lunch today", post.content());
}

pub struct Post {
    content: String,
}

impl Post {
    pub fn new() -> DraftPost {
        DraftPost {
            content: String::new(),
        }
    }

    pub fn content(&self) -> &str {
        &self.content
    }
}

pub struct DraftPost {
    content: String,
}

impl DraftPost {
    pub fn add_text(&mut self, text: &str) {
        self.content.push_str(text);
    }

    pub fn request_review(self) -> PendingReviewPost {
        PendingReviewPost {
            content: self.content,
        }
    }
}

pub struct PendingReviewPost {
    content: String,
}

impl PendingReviewPost {
    pub fn approve(self) -> Post {
        Post {
            content: self.content,
        }
    }
}
```

## Associated types

Properties:

- Associated types connect a type placeholder with a trait such that the trait method definitions can use these placeholders in their signatures.
- Reduces the need to specify generic types in function implementation signatures

Example trait:

```rust
pub trait Iterator {
    type Item; // associated/placeholder type, not the same concept as when using type aliases!

    fn next(&mut self) -> Option<Self::Item>;
}
```

Full example (bad) (from <https://doc.rust-lang.org/rust-by-example/generics/assoc_items/the_problem.html>):

```rust
struct Container(i32, i32);

// A trait which checks if 2 items are stored inside of container.
// Also retrieves first or last value.
trait Contains<A, B> {
    fn contains(&self, _: &A, _: &B) -> bool; // Explicitly requires `A` and `B`.
    fn first(&self) -> i32; // Doesn't explicitly require `A` or `B`.
    fn last(&self) -> i32;  // Doesn't explicitly require `A` or `B`.
}

impl Contains<i32, i32> for Container {
    // True if the numbers stored are equal.
    fn contains(&self, number_1: &i32, number_2: &i32) -> bool {
        (&self.0 == number_1) && (&self.1 == number_2)
    }

    // Grab the first number.
    fn first(&self) -> i32 { self.0 }

    // Grab the last number.
    fn last(&self) -> i32 { self.1 }
}

// `C` contains `A` and `B`. In light of that, having to express `A` and
// `B` again is a nuisance.
fn difference<A, B, C>(container: &C) -> i32 where
    C: Contains<A, B> {
    container.last() - container.first()
}

fn main() {
    let number_1 = 3;
    let number_2 = 10;

    let container = Container(number_1, number_2);

    println!("Does container contain {} and {}: {}",
        &number_1, &number_2,
        container.contains(&number_1, &number_2));
    println!("First number: {}", container.first());
    println!("Last number: {}", container.last());

    println!("The difference is: {}", difference(&container));
}

```

Full example (good) (from <https://doc.rust-lang.org/rust-by-example/generics/assoc_items/types.html>):

```rust
struct Container(i32, i32);

// A trait which checks if 2 items are stored inside of container.
// Also retrieves first or last value.
trait Contains {
    // Define generic types here which methods will be able to utilize.
    type A;
    type B;

    fn contains(&self, _: &Self::A, _: &Self::B) -> bool;
    fn first(&self) -> i32;
    fn last(&self) -> i32;
}

impl Contains for Container {
    // Specify what types `A` and `B` are. If the `input` type
    // is `Container(i32, i32)`, the `output` types are determined
    // as `i32` and `i32`.
    type A = i32;
    type B = i32;

    // `&Self::A` and `&Self::B` are also valid here.
    fn contains(&self, number_1: &i32, number_2: &i32) -> bool {
        (&self.0 == number_1) && (&self.1 == number_2)
    }
    // Grab the first number.
    fn first(&self) -> i32 { self.0 }

    // Grab the last number.
    fn last(&self) -> i32 { self.1 }
}

fn difference<C: Contains>(container: &C) -> i32 {
    container.last() - container.first()
}

fn main() {
    let number_1 = 3;
    let number_2 = 10;

    let container = Container(number_1, number_2);

    println!("Does container contain {} and {}: {}",
        &number_1, &number_2,
        container.contains(&number_1, &number_2));
    println!("First number: {}", container.first());
    println!("Last number: {}", container.last());

    println!("The difference is: {}", difference(&container));
}
```

Summary of full example: Notice how functions that use the trait `Contains` are no longer required to express A or B at all:

```rust
// Without using associated types
fn difference<A, B, C: Contains<A,B>>(container: &C) -> i32 { ... }

// Using associated types
fn difference<C: Contains>(container: &C) -> i32 { ... }
```

## Fully Qualified Syntax for Disambiguation: Calling Methods with the Same Name

Example 1: Specifying which trait's method we want to call

```rust
trait Pilot {
    fn fly(&self);
}

trait Wizard {
    fn fly(&self);
}

struct Human;

impl Pilot for Human {
    fn fly(&self) {
        println!("This is your captain speaking.");
    }
}

impl Wizard for Human {
    fn fly(&self) {
        println!("Up!");
    }
}

impl Human {
    fn fly(&self) {
        println!("*waving arms furiously*");
    }
}

fn main() {
    let person = Human;
    Pilot::fly(&person);
    Wizard::fly(&person);
    person.fly();
}
```

Example 2: Using fully qualified syntax to specify that we want to call the baby_name function from the Animal trait as implemented on Dog

```rust
trait Animal {
    fn baby_name() -> String;
}

struct Dog;

impl Dog {
    fn baby_name() -> String {
        String::from("Spot")
    }
}

impl Animal for Dog {
    fn baby_name() -> String {
        String::from("puppy")
    }
}

fn main() {
    // does not work: Animal::baby_name() is an associated function (not a method),
    // it therefore does not have a self parameter
    // println!("A baby dog is called a {}", Animal::baby_name());

    println!("A baby dog is called a {}", <Dog as Animal>::baby_name());
}
```

In general, fully qualified syntax is defined as follows:

```rust
<Type as Trait>::function(receiver_if_method, next_arg, ...);
```

## Using supertraits

```rust
trait OutlinePrint: fmt::Display {
    fn outline_print(&self) {
        let output = self.to_string();
        let len = output.len();
        println!("{}", "*".repeat(len + 4));
        println!("*{}*", " ".repeat(len + 2));
        println!("* {} *", output);
        println!("*{}*", " ".repeat(len + 2));
        println!("{}", "*".repeat(len + 4));
    }
}

struct Point {
    x: i32,
    y: i32,
}

impl OutlinePrint for Point {}

use std::fmt;

impl fmt::Display for Point {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "({}, {})", self.x, self.y)
    }
}

fn main() {
    let p = Point { x: 1, y: 3 };
    p.outline_print();
}
```

## Using the Newtype Pattern to Implement External Traits on External Types

Properties:

- Circumvents orphan rule: Implement a trait on a type where neither the trait nor the type are local to our crate
- Uses a tuple struct
- Does not (directly) have the methods of the wrapped type, solutions:
  - Either implement `Deref` trait
  - Implement needed methods manually

```rust
use std::fmt;

struct Wrapper(Vec<String>);

impl fmt::Display for Wrapper {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "[{}]", self.0.join(", "))
    }
}

fn main() {
    let w = Wrapper(vec![String::from("hello"), String::from("world")]);
    println!("w = {}", w);
}
```


# Lifetimes

- Lifetime annotations do _not_ change how long a variable/reference will live.
- Having a function returning a value reference (e.g. `&str`) implies that it references one of the input parameters. (It can't be a value instantiated in the function body itself, since that value goes out of scope at the end of the function.)

## Lifetime Annotations in Function Signatures

Example:

```rust
fn main() {
    let string1 = String::from("long string is long");

    {
        let string2 = String::from("xyz");
        let result = longest(string1.as_str(), string2.as_str());
        println!("The longest string is {}", result);
    }
}

fn longest<'a>(x: &'a str, y: &'a str) -> &'a str {
    if x.len() > y.len() {
        x
    } else {
        y
    }
}
```

Remark: The lifetime `'a` will be the smaller of the lifetimes of `x` and `y`.


## Lifetime Annotations in Struct and Method Definitions

Example:

```rust
struct ImportantExcerpt<'a> {
    // an instance of ImportantExcerpt can’t outlive the reference it holds in its part field
    part: &'a str,
}

impl<'a> ImportantExcerpt<'a> {
    // &self does not need a lifetime annotation due to first elision rule
    fn level(&self) -> i32 {
        3
    }
}

impl<'a> ImportantExcerpt<'a> {
    // no lifetime annotations required due to first and third elision rules
    fn announce_and_return_part(&self, announcement: &str) -> &str {
        println!("Attention please: {}", announcement);
        self.part
    }
}


fn main() {
    let novel = String::from("Call me Ishmael. Some years ago...");

    let first_sentence: &str = novel.split('.').next().expect("Could not find a '.'");
    let i = ImportantExcerpt {
        part: first_sentence,
    };
}
```


## Lifetime Elision

There are 3 rules:

1. Each parameter that is a reference gets its own lifetime parameter.
  - `fn foo<'a, 'b>(x: &'a i32, y: &'b i32)`
2. If there is exactly one input lifetime parameter, that lifetime is assigned to all output lifetime parameters.
  - `fn foo<'a>(x: &'a i32) -> &'a i32`
3. If there are multiple input lifetime parameters, but one of them is `&self` or `&mut self` because this is a method, the lifetime of `self` is assigned to all output lifetime parameters.

Example where we want to prevent application of rule 3:

```rust
struct Regex<'a> {
    bar: &'a str,
}

impl<'a> Regex<'a> {
    // use lifetime 'b instead of implicit one from &self for return value
    fn foo<'b>(&self, target: &'b str) -> &'b str {
        target
    }
}
```

Large example combining generic type parameters, trait bounds and lifetimes:

```rust
fn main() {
    let string1 = String::from("abcd");
    let string2 = "xyz";

    let result = longest_with_an_announcement(
        string1.as_str(),
        string2,
        "Today is someone's birthday!",
    );
    println!("The longest string is {}", result);
}

use std::fmt::Display;

fn longest_with_an_announcement<'a, T>(
    x: &'a str,
    y: &'a str,
    ann: T,
) -> &'a str
where
    T: Display,
{
    println!("Announcement! {}", ann);
    if x.len() > y.len() {
        x
    } else {
        y
    }
}
```


## Static lifetime

The `'static` lifetime means that this reference can life for the entire duration of a program. All string literals have it.

Example:

```rust
#![allow(unused)]
fn main() {
    let s: &'static str = "I have a static lifetime.";
}
```

# Tests

## Running tests

Running tests:

```bash
cargo test
```

Running only some tests:

```bash
cargo test part_of_fn_name
```

## Defining test functions

Rust knows two kinds of tests:

1. Unit tests: Testing one module at a time
2. Integration tests: (Potentially) testing multiple modules per test, only using the public interface

Annotate test functions:

```rust
#[test]
fn some_test_func() {
    ...
}
```

Useful macros:

- `assert!` (argument must evaluate to `true`)
- `assert_eq!`, `assert_ne!` (needs traits `PartialEq`, `Debug` (for structs, enums add `#[derive(PartialEq, Debug)]`))

Custom error message in `assert!`:

```rust
assert!(
    result.contains("Carol"),
    "Greeting did not contain name, value was `{}`",
    result
);
```

Useful attributes:

- `#[should_panic]` / `#[should_panic(expected = "Guess value must be less than or equal to 100")]`
- `#[ignore]: Ignore this (expensive) test when running `cargo test` (test these using `cargo test -- --ignored`)

## Unit tests

- Split off tests into its own module, annotate it with `[cfg(test)]`
- Annotate test functions with `#[test]`

Example testing a private function (unit test):

```rust
pub fn add_two(a: i32) -> i32 {
    internal_adder(a, 2)
}

fn internal_adder(a: i32, b: i32) -> i32 {
    a + b
}

#[cfg(test)]
mod tests {
    use super::*; // bring all of the `test` module's parent's items into scope

    #[test]
    fn internal() {
        assert_eq!(4, internal_adder(2, 2));
    }
}
```

## Integrations tests

- Put them into directory `tests` (next to `src`)
- Annotating code with `#[cfg(test)]` is not needed due to special treatment of `tests` directory
- Put helper code into `tests/some_dir/some_file.rs`, e.g. `tests/common/mod.rs`

Example:

```rust
use adder;

mod common;

#[test]
fn it_adds_two() {
    common::setup();
    assert_eq!(4, adder::add_two(2));
}
```

# Packages, Crates and Modules

```plantuml
@startuml
scale 1

class "Package" as p
abstract class "Crate" as c
class "Library Crate" as lc
class "Binary Crate" as bc
class "Module" as m

p "1" o-- "1..*" c
c <|-- lc
c <|-- bc
c "1 " o- "0..*" m
p "1" o- "0..1" lc
p "1" o- "0..*" bc
m "1" o- "0..*" m

note top of p: contains Cargo.toml
note top of m: controls scope and privacy of paths
note bottom of lc: Path for crate with same\nname as package: src/lib.rs
note bottom of bc: Path for crate with same\nname as package: src/main.rs
@enduml
```

Example using absolute and relative paths:

```rust
mod front_of_house {
    pub mod hosting {
        pub fn add_to_waitlist() {}
    }
}

pub fn eat_at_restaurant() {
    // Absolute path
    crate::front_of_house::hosting::add_to_waitlist();

    // Relative path
    front_of_house::hosting::add_to_waitlist();
}
```

Example using `super::`:

```rust
fn serve_order() {}

mod back_of_house {
    fn fix_incorrect_order() {
        cook_order();
        super::serve_order();
    }

    fn cook_order() {}
}
```

Example using struct with private field:

```rust
mod back_of_house {
    pub struct Breakfast {
        pub toast: String,
        seasonal_fruit: String, // private field
    }

    impl Breakfast {
        pub fn summer(toast: &str) -> Breakfast { // public associated function is needed here due to private field in `Breakfast`
            Breakfast {
                toast: String::from(toast),
                seasonal_fruit: String::from("peaches"),
            }
        }
    }
}

pub fn eat_at_restaurant() {
    // Order a breakfast in the summer with Rye toast
    let mut meal = back_of_house::Breakfast::summer("Rye");

    // Change our mind about what bread we'd like
    meal.toast = String::from("Wheat");
    println!("I'd like {} toast please", meal.toast);

    // The next line won't compile if we uncomment it; we're not allowed
    // to see or modify the seasonal fruit that comes with the meal
    // meal.seasonal_fruit = String::from("blueberries");
}
```

Example using public enum (note that enum variants by default are public):

```rust
mod back_of_house {
    pub enum Appetizer {
        Soup,
        Salad,
    }
}

pub fn eat_at_restaurant() {
    let order1 = back_of_house::Appetizer::Soup;
    let order2 = back_of_house::Appetizer::Salad;
}
```

## Using `use`

```rust
mod front_of_house {
    pub mod hosting {
        pub fn add_to_waitlist() {}
    }
}

// good style: don't bring function itself into scope
use crate::front_of_house::hosting; // Variant 1: using absolute path
use self::front_of_house::hosting;  // Variant 2: using relative path

// for structs, enum, etc. it's fine to bring themselves into scope
use std::collections::HashMap;
let mut map = HashMap::new();
map.insert(1, 2);

pub fn eat_at_restaurant() {
    hosting::add_to_waitlist();
}
```

## Using `use ... as`

```rust
use std::fmt::Result;
use std::io::Result as IoResult;

fn function1() -> Result {
    // --snip--
}

fn function2() -> IoResult<()> {
    // --snip--
}
```

## Using `pub use` (re-exporting)

```rust
mod front_of_house {
    pub mod hosting {
        pub fn add_to_waitlist() {}
    }
}

// allow external code to call `hosting::add_to_waitlist()`
pub use crate::front_of_house::hosting;

pub fn eat_at_restaurant() {
    hosting::add_to_waitlist();
}
```

## Using nested paths

```rust
// Long variant
use std::cmp::Ordering;
use std::io;

// Short variant
use std::{cmp::Ordering, io};
```

Also:

```rust
// Long variant
use std::io;
use std::io::Write;

// Short variant
use std::io::{self, Write};
```

And globbing (primarily useful when doing testing):

```rust
use std::collections::*;
```

## Splitting modules into different files

This is the root file `src/lib.rs` (or `src/main.rs`):

```rust
mod front_of_house; // read file `src/front_of_house.rs`

pub use crate::front_of_house::hosting;
```

This is file `src/front_of_house.rs`:

```rust
pub mod hosting;
```

This is file `src/front_of_house/hosting.rs`:

```rust
pub fn add_to_waitlist() {}
```

# Closures

Properties:

- Small, anonymous functions
- Can be assigned to variables (The variable then holds the function definition, not the result of the function evaluation.)
- A closure implements at least one of the following traits:
  - `Fn`: borrow values from environment _immutably_
  - `FnMut`: borrow values from environment _mutably_
  - `FnOnce`: consumes the variables it captures by taking ownership, can therefore be called only once

Syntaxes:

```rust
fn  add_one_v1   (x: u32) -> u32 { x + 1 }
let add_one_v2 = |x: u32| -> u32 { x + 1 };
let add_one_v3 = |x|             { x + 1 };
let add_one_v4 = |x|               x + 1  ;
```

Example: Simulated slow function

```rust
let expensive_closure = |num| {
    println!("calculating slowly...");
    thread::sleep(Duration::from_secs(2));
    num
};
```

Example: My own implementation of Cacher (see https://doc.rust-lang.org/stable/book/ch13-01-closures.html#limitations-of-the-cacher-implementation )

TODO: Take from https://jira.swissphone.com/browse/IRIS-112

Example: `map()`

```rust
let v1: Vec<i32> = vec![1, 2, 3];
let v2: Vec<_> = v1.iter().map(|x| x + 1).collect();
assert_eq!(v2, vec![2, 3, 4]);
```

Example: `filter()`

```rust
#[derive(PartialEq, Debug)]
struct Shoe {
    size: u32,
    style: String,
}

fn shoes_in_size(shoes: Vec<Shoe>, shoe_size: u32) -> Vec<Shoe> {
    shoes.into_iter().filter(|s| s.size == shoe_size).collect()
}
```

Generating a closure in a function:

```rust
fn returns_closure() -> Box<dyn Fn(i32) -> i32> {
    Box::new(|x| x + 1)
}
```


# Iterators

Get iterator for a collection:

```rust
let v1: Vec<i32> = vec![1, 2, 3];
let mut i = v1.iter();
```

Consume all elements from an iterator:

```rust
// this runs as long as `pop()` does not return `None`
while let Some(top) = stack.pop() {
    println!("{}", top);
}
```

Interesting methods on interator (see <https://doc.rust-lang.org/std/iter/trait.Iterator.html>):

- Iterating in some way:
  - `next()`
  - `peekable()`
  - `count()`
  - `enumerate()`
- Reduce number of processed elements:
  - `last()`
  - `advance_by()`
  - `nth()`
  - `step_by()`
  - `filter(closure)`: Create iterator that only returns element where closure returns true
  - `filter_map()` (duplicate)
  - `take()`
  - `take_while()`
  - `skip()`
  - `skip_while()`
  - `map_while()` (duplicate)
- Increase number of elements:
  - `intersperse()`
  - `intersperse_with()`
- Combining with other iterator:
  - `chain(i2)`: Returns new iterator that iterates first over itself, than over i2
  - `zip(i2)`: Returns a tuple of combinations from two iterators
- Call closure on elements:
  - `map(closure)`: Create iterator which calls closure on each element (lazy evaluation)
  - `for_each()`: (Immediately) call function for each element
  - `filter_map` (duplicate)
  - `map_while()` (duplicate)
- Stateful:
  - `scan()`

TODO: continue after `scan()`


- `filter(closure)`: Create iterator that only returns element where closure returns true

# Smart pointers

Common properties:

- Implement trait `Deref` (i.e. behaviour when dereferencing using `*`)
- Implement trait `Drop`

## `Box<T>`

Properties:

- Heap allocation
- Known size (by usage of pointer)
- Indirection (via pointer)

Example:

```rust
use crate::List::{Cons, Nil};

enum List {
    Cons(i32, Box<List>),
    Nil,
}

fn main() {
    let list = Cons(1, Box::new(Cons(2, Box::new(Cons(3, Box::new(Nil))))));
}
```

## `Deref` trait

Example:

```rust
use std::ops::Deref;

impl<T> Deref for MyBox<T> {
    type Target = T;

    fn deref(&self) -> &T {
        &self.0
    }
}

struct MyBox<T>(T);

impl<T> MyBox<T> {
    fn new(x: T) -> MyBox<T> {
        MyBox(x)
    }
}

fn hello(name: &str) {
    println!("Hello, {}!", name);
}

fn main() {
    // part 1
    let m = MyBox::new(String::from("Rust"));
    hello(&m);

    // part 2
    let x = 5;
    let y = MyBox::new(x);

    assert_eq!(5, x);
    assert_eq!(5, *y);
}
```

## `Drop` trait

Properties:

- `drop()` gets called when a value goes out of scope
- usually used to release ressources such as files or network connections

Example:

```rust
struct CustomSmartPointer {
    data: String,
}

impl Drop for CustomSmartPointer {
    fn drop(&mut self) {
        println!("Dropping CustomSmartPointer with data `{}`!", self.data);
    }
}

fn main() {
    let c = CustomSmartPointer {
        data: String::from("some data"),
    };
    println!("CustomSmartPointer created.");
    drop(c); // this is std::mem::drop (included in the prelude)
    // calling c.drop() manually is not allowed
    println!("CustomSmartPointer dropped before the end of main.");
}
```

## `Rc<T>`

Properties:

- Keeps track of the number of references to data on heap (using `strong_count`)
- Used for allocation of data on the heap when multiple program parts/owners want to read
- Only used for single-threaded scenarios
- `Rc<T>` instance will be cleaned up if its `strong_count` is 0
- Risk of memory leaks (increased by using `RefCell<T>`)

Example:

```rust
use crate::List::{Cons, Nil};
use std::rc::Rc;

enum List {
    Cons(i32, Rc<List>),
    Nil,
}

fn main() {
    let a = Rc::new(Cons(5, Rc::new(Cons(10, Rc::new(Nil)))));
    let b = Cons(3, Rc::clone(&a)); // Rc::clone() does a shallow copy!
    let c = Cons(4, Rc::clone(&a));

    println!("number of references: {}", Rc::strong_count(&a))
}
```

## `RefCell<T>`

Properties:

- `RefCell<T>` has single owner
- allows immutable and mutable borrows checked at runtime
- value inside `RefCell<T>` can be mutated even when `RefCell<T>` is immutable (_interior mutability pattern_)
- Risk of panics due to checking borrowing rules at runtime

Example: Using `RefCell<T>` to mutate an inner value while the outer value is considered immutable

```rust
pub trait Messenger {
    fn send(&self, msg: &str);
}

pub struct LimitTracker<'a, T: Messenger> {
    messenger: &'a T,
    value: usize,
    max: usize,
}

impl<'a, T> LimitTracker<'a, T>
where
    T: Messenger,
{
    pub fn new(messenger: &T, max: usize) -> LimitTracker<T> {
        LimitTracker {
            messenger,
            value: 0,
            max,
        }
    }

    pub fn set_value(&mut self, value: usize) {
        self.value = value;

        let percentage_of_max = self.value as f64 / self.max as f64;

        if percentage_of_max >= 1.0 {
            self.messenger.send("Error: You are over your quota!");
        } else if percentage_of_max >= 0.9 {
            self.messenger
                .send("Urgent warning: You've used up over 90% of your quota!");
        } else if percentage_of_max >= 0.75 {
            self.messenger
                .send("Warning: You've used up over 75% of your quota!");
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::cell::RefCell;

    struct MockMessenger {
        sent_messages: RefCell<Vec<String>>,
    }

    impl MockMessenger {
        fn new() -> MockMessenger {
            MockMessenger {
                sent_messages: RefCell::new(vec![]),
            }
        }
    }

    impl Messenger for MockMessenger {
        fn send(&self, message: &str) {
            self.sent_messages.borrow_mut().push(String::from(message));
        }
    }

    #[test]
    fn it_sends_an_over_75_percent_warning_message() {
        // --snip--
        let mock_messenger = MockMessenger::new();
        let mut limit_tracker = LimitTracker::new(&mock_messenger, 100);

        limit_tracker.set_value(80);

        assert_eq!(mock_messenger.sent_messages.borrow().len(), 1);
    }
}
```

Example: Combining `RefCell<T>` and `Rc<T>` in order to get a value that can have multiple owners and that you can mutate

```rust
#[derive(Debug)]
enum List {
    Cons(Rc<RefCell<i32>>, Rc<List>),
    Nil,
}

use crate::List::{Cons, Nil};
use std::cell::RefCell;
use std::rc::Rc;

fn main() {
    let value = Rc::new(RefCell::new(5));

    let a = Rc::new(Cons(Rc::clone(&value), Rc::new(Nil)));

    let b = Cons(Rc::new(RefCell::new(3)), Rc::clone(&a));
    let c = Cons(Rc::new(RefCell::new(4)), Rc::clone(&a));

    *value.borrow_mut() += 10;

    println!("a after = {:?}", a);
    println!("b after = {:?}", b);
    println!("c after = {:?}", c);
}
```

## `Weak<T>`

Properties:

- Corresponds to a _weak reference_
- Can be created by calling `Rc::downgrade` and passing a reference to `Rc<T>`. It will return a smart pointer of type `Weak<T>` and increase `weak_count` by 1.
- A `Rc<T>` instance can be cleaned up even if `weak_count` is larger than 0

# Concurrency

Example:

```rust
use std::thread;
use std::time::Duration;

fn main() {
    let handle = thread::spawn(|| {
        for i in 1..10 {
            println!("hi number {} from the spawned thread!", i);
            thread::sleep(Duration::from_millis(1));
        }
    });

    for i in 1..5 {
        println!("hi number {} from the main thread!", i);
        thread::sleep(Duration::from_millis(1));
    }

    handle.join().unwrap();
}
```

Example:

```rust
use std::thread;
use std::time::Duration;

fn main() {
    let handle = thread::spawn(|| {
        for i in 1..10 {
            println!("hi number {} from the spawned thread!", i);
            thread::sleep(Duration::from_millis(1));
        }
    });

    for i in 1..5 {
        println!("hi number {} from the main thread!", i);
        thread::sleep(Duration::from_millis(1));
    }

    handle.join().unwrap();
}
```

Example:

```rust
use std::thread;

fn main() {
    let v = vec![1, 2, 3];

    // move makes the closure taking ownership of v
    let handle = thread::spawn(move || {
        println!("Here's a vector: {:?}", v);
    });

    handle.join().unwrap();
}
```

## Using message passing

Example:

```rust
use std::sync::mpsc;
use std::thread;

fn main() {
    let (tx, rx) = mpsc::channel();

    thread::spawn(move || {
        let val = String::from("hi");
        tx.send(val).unwrap(); // send() returns Result<T, E>
	// can't use val here anymore
    });

    let received = rx.recv().unwrap(); // recv() is blocking, one could also use try_recv()
                                       // recv() returns Result<T, E>
    println!("Got: {}", received);
}
```

Example:

```rust
use std::sync::mpsc;
use std::thread;
use std::time::Duration;

fn main() {
    let (tx, rx) = mpsc::channel();

    let tx1 = tx.clone();
    thread::spawn(move || {
        let vals = vec![
            String::from("hi"),
            String::from("from"),
            String::from("the"),
            String::from("thread"),
        ];

        for val in vals {
            tx1.send(val).unwrap();
            thread::sleep(Duration::from_secs(1));
        }
    });

    thread::spawn(move || {
        let vals = vec![
            String::from("more"),
            String::from("messages"),
            String::from("for"),
            String::from("you"),
        ];

        for val in vals {
            tx.send(val).unwrap();
            thread::sleep(Duration::from_secs(1));
        }
    });

    for received in rx { // treat rx as iterator; iterations ends when channel is closed
        println!("Got: {}", received);
    }
}
```

## Sharing state using `Mutex<T>` and `Arc<T>`

Properties:

- `Mutex<T>` provides interior mutability, comparable to `RefCell<T>`
- Singlethreaded case: `Rc::new(RefCell::new(0))`
- Multithreaded case: `Arc::new(Mutex::new(0))`

The following example uses `Arc<T>`, standing for _atomically reference counted type_.

Example:

```rust
use std::sync::{Arc, Mutex};
use std::thread;

fn main() {
    let counter = Arc::new(Mutex::new(0));
    let mut handles = vec![];

    for _ in 0..10 {
        let counter = Arc::clone(&counter);
        let handle = thread::spawn(move || {
            let mut num = counter.lock().unwrap();

            *num += 1;
        });
        handles.push(handle);
    }

    for handle in handles {
        handle.join().unwrap();
    }

    println!("Result: {}", *counter.lock().unwrap());
}
```
# Patterns and matching

## Conditional `if let` expressions

Properties:

- No check for exhaustiveness
- Does not drop temporary values until the end of the associated block (might be relevent e.g. for multi-threaded programs)

```rust
fn main() {
    let favorite_color: Option<&str> = None;
    let is_tuesday = false;
    let age: Result<u8, _> = "34".parse();

    if let Some(color) = favorite_color {
        println!("Using your favorite color, {}, as the background", color);
    } else if is_tuesday {
        println!("Tuesday is green day!");
    } else if let Ok(age) = age { // this introduces a shadowed variable `age`
        if age > 30 {
            println!("Using purple as the background color");
        } else {
            println!("Using orange as the background color");
        }
    } else {
        println!("Using blue as the background color");
    }
}
```

## `while let`

Properties:

- Can be used to consume an iterator
- Does not drop temporary values until the end of the associated block (might be relevent e.g. for multi-threaded programs)

```rust
fn main() {
    let mut stack = Vec::new();

    stack.push(1);
    stack.push(2);
    stack.push(3);

    // this runs as long as `pop()` does not return `None`
    while let Some(top) = stack.pop() {
        println!("{}", top);
    }
}
```

## Function parameters as patterns

```rust
fn print_coordinates(&(x, y): &(i32, i32)) {
    println!("Current location: ({}, {})", x, y);
}

fn main() {
    let point = (3, 5);
    print_coordinates(&point);
}
```

## `match` with multiple patterns

```rust
fn main() {
    let x = 1;

    match x {
        1 | 2 => println!("one or two"),
        3 => println!("three"),
        _ => println!("anything"),
    }
}
```

## `match` with range of values

The `..=` syntax allows us to match to an inclusive range of values.

```rust
fn main() {
    let x = 'c';

    match x {
        'a'..='j' => println!("early ASCII letter"),
        'k'..='z' => println!("late ASCII letter"),
        _ => println!("something else"),
    }
}
```

## Destructuring structs

Variant 1: Using other variable names (verbose):

```rust
struct Point {
    x: i32,
    y: i32,
}

fn main() {
    let p = Point { x: 0, y: 7 };

    let Point { x: a, y: b } = p;
    assert_eq!(0, a);
    assert_eq!(7, b);
}
```

Variant 2: Using same variable names (shorter):

```rust
struct Point {
    x: i32,
    y: i32,
}

fn main() {
    let p = Point { x: 0, y: 7 };

    let Point { x, y } = p;
    assert_eq!(0, x);
    assert_eq!(7, y);
}
```

Variant 3: Separate different cases based on struct values:

```rust
fn main() {
    let p = Point { x: 0, y: 7 };

    match p {
        Point { x, y: 0 } => println!("On the x axis at {}", x),
        Point { x: 0, y } => println!("On the y axis at {}", y),
        Point { x, y } => println!("On neither axis: ({}, {})", x, y),
    }
}
```

## Destructuring enums and structs

Destructuring a flat enum:

```rust
enum Message {
    Quit,
    Move { x: i32, y: i32 },
    Write(String),
    ChangeColor(i32, i32, i32),
}

fn main() {
    let msg = Message::ChangeColor(0, 160, 255);

    match msg {
        Message::Quit => {
            println!("The Quit variant has no data to destructure.")
        }
        Message::Move { x, y } => {
            println!(
                "Move in the x direction {} and in the y direction {}",
                x, y
            );
        }
        Message::Write(text) => println!("Text message: {}", text),
        Message::ChangeColor(r, g, b) => println!(
            "Change the color to red {}, green {}, and blue {}",
            r, g, b
        ),
    }
}
```

Destructuring a nested enum:

```rust
enum Color {
    Rgb(i32, i32, i32),
    Hsv(i32, i32, i32),
}

enum Message {
    Quit,
    Move { x: i32, y: i32 },
    Write(String),
    ChangeColor(Color),
}

fn main() {
    let msg = Message::ChangeColor(Color::Hsv(0, 160, 255));

    match msg {
        Message::ChangeColor(Color::Rgb(r, g, b)) => println!(
            "Change the color to red {}, green {}, and blue {}",
            r, g, b
        ),
        Message::ChangeColor(Color::Hsv(h, s, v)) => println!(
            "Change the color to hue {}, saturation {}, and value {}",
            h, s, v
        ),
        _ => (),
    }
}
```

Destructuring Structs and Tuples:

```rust
fn main() {
    struct Point {
        x: i32,
        y: i32,
    }

    let ((feet, inches), Point { x, y }) = ((3, 10), Point { x: 3, y: -10 });
    # makes available the variables feet, inches, x, y
}
```

## Ignoring values in a pattern

Ignoring a function parameter:

```rust
fn foo(_: i32, y: i32) {
    println!("This code only uses the y parameter: {}", y);
}

fn main() {
    foo(3, 4);
}
```

Ignoring parts of a value within `Some()`:

```rust
fn main() {
    // existing setting, may be None
    let mut setting_value = Some(5);

    // user can set new value or choose unset, but only if existing value is unset
    let new_setting_value = Some(10);

    match (setting_value, new_setting_value) {
        (Some(_), Some(_)) => {
            println!("Can't overwrite an existing customized value");
        }
        _ => {
            setting_value = new_setting_value;
        }
    }

    println!("setting is {:?}", setting_value);
}
```

Destructuring a tuple while ignoring some values:

```rust
fn main() {
    let numbers = (2, 4, 8, 16, 32);

    match numbers {
        (first, _, third, _, fifth) => {
            println!("Some numbers: {}, {}, {}", first, third, fifth)
        }
    }
}
```

Ignoring unused variables:

```rust
fn main() {
    let _x = 5;
    let y = 10;
}
```

Underscore vs. underscored name:

```rust
fn main() {
    let s = Some(String::from("Hello!"));

    // using Some(_) is fine, using Some(_s) would move s,
    // make it unavailable afterwards
    if let Some(_) = s {
        println!("found a string");
    }

    println!("{:?}", s); // s would nout be available here
}
```

Ignoring remaining parts of structs or tuples:

```rust
fn main() {
    struct Point {
        x: i32,
        y: i32,
        z: i32,
    }

    let origin = Point { x: 0, y: 0, z: 0 };

    match origin {
        Point { x, .. } => println!("x is {}", x),
    }
}
```

```rust
fn main() {
    let numbers = (2, 4, 8, 16, 32);

    match numbers {
        (first, .., last) => {
            println!("Some numbers: {}, {}", first, last);
        }
    }
}
```

## Match guards

Properties:

- additional condition in a `match` arm

```rust
fn main() {
    let num = Some(4);

    match num {
        Some(x) if x < 5 => println!("less than five: {}", x),
        Some(x) => println!("{}", x),
        None => (),
    }
}
```

## The @ operator (at)

Properties:

- binds to a value
- allows for testing

```rust
fn main() {
    enum Message {
        Hello { id: i32 },
    }

    let msg = Message::Hello { id: 5 };

    match msg {
        Message::Hello {
            id: id_variable @ 3..=7,
        } => println!("Found an id in range: {}", id_variable),
        Message::Hello { id: 10..=12 } => {
            println!("Found an id in another range")
        }
        Message::Hello { id } => println!("Found some other id: {}", id),
    }
}
```

# Unsafe Rust

Using the `unsafe` keyword allows the following 5 actions:

1. Dereference a raw pointer
2. Call an unsafe function or method
3. Access or modify a mutable static variable
4. Implement an unsafe trait
5. Access fields of unions

## Dereferencing a raw pointer

There are two kinds of raw pointers:

- mutable raw pointer: `*const T`
- immutable raw pointer: `*mut T`

```rust
fn main() {
    let mut num = 5;

    let r1 = &num as *const i32;   // immutable
    let r2 = &mut num as *mut i32; // mutable

    // having a mutable and immutable reference at the same time
    // is not possible using safe rust, therefore `unsafe` is needed
    unsafe {
        println!("r1 is: {}", *r1);
	// *r1 += 1; // not possible, r1 is immutable
        *r2 += 1;
        println!("r2 is: {}", *r2);
    }
}
```

## Using unsafe functions

General syntax:

```rust
unsafe fn dangerous() {
    /* do something dangeours here, no further unsafe block is needed for that */
}

fn main() {

    unsafe {
        dangerous();
    }
}
```

Calling C code from Rust:

```rust
extern "C" {
    fn abs(input: i32) -> i32;
}

fn main() {
    unsafe {
        println!("Absolute value of -3 according to C: {}", abs(-3));
    }
}
```

Calling Rust code from C:

```rust
#[no_mangle] // the compiler should not change this function name
pub extern "C" fn call_from_c() {
    println!("Just called a Rust function from C!");
}
```

## Accessing or modifying a mutable static variable

Properties of static (global) variables:

- Fixed address in memory (constants are allowed to duplicate their data)
- Can be mutable (constants are not)
- Accessing an immutable static variable is safe.
- Modifying a mutable static variable is unsafe.

Using a global (in Rust called: static) variable:

```rust
static HELLO_WORLD: &str = "Hello, world!";

fn main() {
    println!("name is: {}", HELLO_WORLD);
}
```

Example:

```rust
static mut COUNTER: u32 = 0;

fn add_to_count(inc: u32) {
    unsafe {
        COUNTER += inc;
    }
}

fn main() {
    add_to_count(3);

    unsafe {
        println!("COUNTER: {}", COUNTER);
    }
}
```

## Implementing an unsafe trait

```rust
unsafe trait Foo {
    // methods go here
}

unsafe impl Foo for i32 {
    // method implementations go here
}

fn main() {}
```

## Accessing fields of a union

See <https://doc.rust-lang.org/stable/reference/items/unions.html>


# Advanced features

## Type alias

Example:

```rust
fn main() {
    type Kilometers = i32;

    let x: i32 = 5;
    let y: Kilometers = 5;

    println!("x + y = {}", x + y);
}
```

Example from `std::io`:

```rust
type Result<T> = std::result::Result<T, std::io::Error>;
```


## Dynamically Sized Types and the Sized Trait

By default, generic functions will work only on types that have a known size at compile time. However, you can use the following special syntax to relax this restriction:

```rust
fn generic<T: ?Sized>(t: &T) {
    // --snip--
}
```

A trait bound on `?Sized` means "T may or may not be Sized" and this notation overrides the default that generic types must have a known size at compile time. The `?Trait` syntax with this meaning is only available for `Sized`, not any other traits.

Also note that we switched the type of the `t` parameter from `T` to `&T`. Because the type might not be `Sized`, we need to use it behind some kind of pointer.


## Function pointers vs. closures

Example 1: Convert vector of numbers to vector of strings (using closure):

```rust
fn main() {
    let list_of_numbers = vec![1, 2, 3];
    let list_of_strings: Vec<String> =
        list_of_numbers.iter().map(|i| i.to_string()).collect();
}

```

Example 2: Convert vector of numbers to vector of strings (using function pointer):

```rust
fn main() {
    let list_of_numbers = vec![1, 2, 3];
    let list_of_strings: Vec<String> =
        list_of_numbers.iter().map(ToString::to_string).collect();
}
```

Short initializer syntax:

```rust
fn main() {
    enum Status {
        Value(u32),
        Stop,
    }

    let list_of_statuses: Vec<Status> = (0u32..20).map(Status::Value).collect();
}
```


# To read

- <https://github.com/bheesham/rust.fish>
- <http://cliffle.com/p/dangerust/>
- <https://exercism.io/tracks/rust>
- <https://dhghomon.github.io/easy_rust>

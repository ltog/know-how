# Glamorous Toolkit

## Getting started

### Fixing the font size

Open gt, click magnification glass, enter `fontsize`, scroll down to `How to scale the UI`, hover and click the checkbox.

Hover the lowermost example and click the triangle ("play") button.

Close the current window and when being asked, choose `Save and quit`.

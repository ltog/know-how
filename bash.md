# bash

## Handling files

### Globbing

Make `*` match also hidden files and hidden directories (but not the special directories `.` and `..`):

```bash
shopt -s dotglob
ls *
```

Remove all directories except one:

```bash
shopt -s extglob
shopt -s dotglob
rmdir !(importantdir)
```

Note: Over SSH you might need to do something like:

```bash
ssh user@$ip -- 'bash -O extglob -c "shopt -s dotglob && rmdir !(importantdir)"'
```

Note: `shopt -s` commands will apply to the current bash session only. You can make the setting permanent by putting `shopt ...` into `.bashrc`.

## Prevent parallel execution of a script

Put this at the top of a bash script (below the shebang) in order to prevent its parallel execution:

### Variant 1: Abort execution of second script

```bash
# prevent parallel executions of this file by aborting subsequent calls
[ "${FLOCKER}" != "$0" ] && exec env FLOCKER="$0" flock -en "$0" "$0" "$@" || :
```

### Variant 2: Delay execution of second script

```bash
# prevent parallel executions of this file by delaying subsequent calls
[ "${FLOCKER}" != "$0" ] && exec env FLOCKER="$0" flock -e "$0" "$0" "$@" || :
```

Source: `man flock` (section EXAMPLES)

# fzf

## Installation

```bash
sudo apt install fzf
```

## Customization

For fish:

```fish
set -Ux FZF_DEFAULT_OPTS '--layout=reverse --height 99% --multi'
```

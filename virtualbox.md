# VirtualBox

## Enabling USB devices

In order to enable USB devices on the guest:

- Install extension pack (NOT guest additions!): File -> Preferences... -> Extensions -> Plus symbol
- Add your user to group `vboxusers`: `sudo usermod -a -G vboxusers $USER && shutdown -r now`

Source: https://forums.virtualbox.org/viewtopic.php?f=35&t=82639

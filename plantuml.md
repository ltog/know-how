# PlantUML cheat sheet

## General

### Changing output size

```plantuml
scale 2
```

## State diagram

### Setting a title

```plantuml
title This is my title
```

### Denoting an initial state

```plantuml
[*] --> READY
```

### Hiding empty descriptions

```plantuml
hide empty description
```

### Adding a comment box

```plantuml
note as n1
This is
my multiline
note.
end note
```

### Changing state background color

```plantuml
skinparam state {
	BackgroundColor<<auxiliary>> White
}

state COUNTING <<auxiliary>>
```

# Wireshark

## Sniff on remote device

### Step 1: Allow normal users to run tcpdump (optional)

```bash
sudo groupadd tcpdump                        # create new group called `tcpdump`
sudo addgroup $USER tcpdump                  # add current user to group `tcpdump`
sudo chgrp tcpdump $(which tcpdump)          # change group of tcpdump binary to tcpdump
sudo chmod g+x $(which tcpdump)              # allow members of group to execute tcpdump binary
sudo setcap cap_net_raw+eip $(which tcpdump) # permit raw access to network interfaces
getcap $(which tcpdump)                      # optional: make sure the capabilities have been set correctly
```

(Tested with Ubuntu 18.04)

TODO: Maybe the capability `CAP_NET_ADMIN` is also needed.

From <https://www.stev.org/post/howtoruntcpdumpasroot>

### Step 2: Make sure tcpdump runs on remote machine

```bash
tcpdump
```

(if necessary, abort with Ctrl-c)

If you get the error message

```
tcpdump: Couldn't find user 'tcpdump'
```

run

```bash
sudo adduser tcpdump --no-create-home --disabled-password # or better --disabled-login ?
```

### Step 3a: Forward network traffic to local Wireshark instance (Linux Client)

Variant 1 (when login to remote machine is possible without entering a password)

```bash
ssh 10.20.30.40 'tcpdump -U -s 0 -i eth0 "not tcp port 22" -w -' | wireshark -k -i -
```

Explanation:

- `tcpdump`: Capture traffic from network card
  - `-U` : Immediately output captured packets
  - `-s 0` : Output untruncated packets. (Alternative: `-s150`: Only forward the first 150 bytes of every packet. This saves bandwidth and may already be enough for debugging.)
  - `"not tcp port 22"`: Skip packets coming from or going to port 22 (these are the result of our SSH connection).
  - `-w -` : Forward the output to stdout (i.e. the SSH connection in our case) 
- `wireshark`
  - `-k`: Start Wireshark immediately
  - `-i -`: Read from stdin (i.e. from SSH)

Variant 2 (when you need to enter a password to log into remote machine)

```bash
mkfifo /tmp/wireshark
wireshark -k -i /tmp/wireshark &
ssh 10.20.30.40 'tcpdump -U -s 0 -i eth0 "not tcp port 22" -w -' > /tmp/wireshark
```

From <https://www.commandlinefu.com/commands/view/4373/analyze-traffic-remotely-over-ssh-w-wireshark>

### Step 3b: Forward network traffic to local Wireshark instance (Windows Client)

```
plink -ssh -pw topsecret -batch root@11.22.33.44 "tcpdump -U -s 0 -i eth0 'not tcp port 22' -w -" | "C:\Program Files\Wireshark\wireshark.exe" -k -i -
```

Explanation:

- `plink`: Command line variant of putty
  - `-ssh` : Use SSH protocol
  - `-pw` : Server password (Probably better alternative: use `pageant`)
  - `-batch` : Disable interactive prompts (they mess around with content in pipe)
- `tcpdump`: Capture traffic from network card
  - `-U` : Immediately output captured packets
  - `-s 0` : Output untruncated packets. (Alternative: `-s150`: Only forward the first 150 bytes of every packet. This saves bandwidth and may already be enough for debugging.)
  - `'not tcp port 22'`: Skip packets coming from or going to port 22 (these are the result of our SSH connection).
  - `-w -` : Forward the output to stdout (i.e. the SSH connection in our case)
- `wireshark.exe` : Run local Wireshark instance
  - `-k`: Start Wireshark immediately
  - `-i -`: Read from stdin (i.e. from SSH)

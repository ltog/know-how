# use 'grepl' instead of 'grep' if files with long lines mess up your console
grepl() {
	$(which grep) --color=always $@ | less -RS
}

# pretty print json (careful: reorders keys!)                                   
# sudo apt install python-pygments                                              
ppj() {                                                                         
        python -m json.tool | pygmentize -l json                                   
}   
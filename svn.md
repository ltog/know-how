# SVN


## Basics

Get latest changes from Server: `svn update` (corresponds to `git pull`)


## Getting information

### Commit history

See the history of commits: `svn log | head` (corresponds to `git log`)

### Repository information

    svn info


## Resolving conflicts

Discard changes you made:

    svn resolve --accept theirs-full sandwich.txt

Source: <https://svnbook.red-bean.com/en/1.6/svn.ref.svn.c.resolve.html>


## Reverting

### Revert to older commited state

From https://stackoverflow.com/a/13330594

#### Undo a single commit

Undo the changes from a certain commit (not necessarily the last one) in your working directory: `svn merge -c -555 .` (corresponds to `git checkout <commit> -- <file>`)

#### Undo a range of commits

See what the change will be: `svn diff -r 55:52`

Apply change to workspace: `svn merge -r 55:52 .`

### Undo local change

Undo change in working directory, i.e. set back working directory to last commited state: `svn revert <myfile>` (corresponds to `git checkout -- <myfile>`)

### Unadd a file

In order to undo an add operation, use:

    svn rm --keep-local thing_to_unadd

from: <https://stackoverflow.com/questions/2906892/unadd-a-file-to-svn-before-commit>

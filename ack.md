# ack

## Common use-cases

Search fixed string:

    ack -Q '* * *'

Search only certain file types:

    ack --php 'preg_replace'

## An .ackrc file

Locate this file at `~/.ackrc`:

```
# improve line number readability on white background
--color-lineno=yellow

# improve filename readability on white background
--color-filename=green
```

# nmap

## Simple ping scan

```bash
sudo nmap -n -PE -sn 10.111.11.208/28 -oG -
```

- `sudo`: if skipped, nmap will use TCP pingscan instead of ICMP (this created wrong results for me)
- `-n`: No DNS lookups
- `-PE`: Use ICMP echo scan (ping)
- `-sn`: Disable port scan
- `-oG -`: Write grepable output to stdout


Options:

- `-v`: Also show scanned hosts that are down

## Show servers running SSH

```bash
nmap --open -Pn -p 22 192.168.1.0/24
```

# Ansible

## Installation

### Global installation

Install Ansible like this:

```bash
python3 -m pip install --user ansible
```

Verify installation:

```bash
ansible --version
python3 -m pip show ansible
```


## Upgrade

```bash
python3 -m pip install --upgrade --user ansible
```

### Installation in venv

```fish
mkdir ansible
cd ansible
python3 -m venv venv-ansible
source venv-ansible/bin/activate.fish
python3 -m pip install --upgrade pip
python3 -m pip install ansible
```

Source: <https://learning.oreilly.com/library/view/ansible-up-and/9781098109141/ch02.html#ansible_development>


### Installation of latest version in venv

```fish
python3 -m venv venv-ansible
source venv-ansible/bin/activate.fish
python3 -m pip install --upgrade pip
pip3 install wheel
git clone https://github.com/ansible/ansible.git --recursive
pip3 install -r ansible/requirements.txt
```

Adjust variables every time:

```fish
cd ./ansible
source ./hacking/env-setup.fish
```


## Prerequisites

### Inventory

An inventory is a collection of all hosts and groups that Ansible manages. In the most simple case it's a static file. Default path is `/etc/ansible/hosts`.

```
[onetwo]
host1 ansible_host=127.0.0.1 ansible_user=vagrant ansible_port=2222 ansible_ssh_private_key_file=./.vagrant/machines/node-1/virtualbox/private_key

host2 ansible_host=127.0.0.1 ansible_user=vagrant ansible_port=2200 ansible_ssh_private_key_file=./.vagrant/machines/node-2/virtualbox/private_key

[three]
host3 ansible_host=127.0.0.1 ansible_user=vagrant ansible_port=2201 ansible_ssh_private_key_file=./.vagrant/machines/node-3/virtualbox/private_key
```

You can display an inventory using:

    ansible-inventory [-i inventory] --graph

Output should look something like:

```
@all:
  |--@onetwo:
  |  |--host1
  |  |--host2
  |--@three:
  |  |--host3
  |--@ungrouped:
```

### Ansible configuration file

Specify a configuration file:

```ini
[defaults]
inventory=./hosts
```

Settings can be put in the following places:

- `ANSIBLE_CONFIG` (an environment variable)
- `ansible.cfg` (in the current directory)
- `.ansible.cfg` (in the home directory)
- `/etc/ansible/ansible.cfg`

Validate using the command:

```bash
ansible-inventory --list
```

## Ad-hoc commands

Important parameters:

- `-i`: Inventory (File with host names), default is `/etc/ansible/hosts`
- `-m`: Module
- `-l`: Limit hosts
- pattern: `all`, `ungrouped` and what has been defined in inventory

Ping all hosts (example of an ad-hoc command):

```bash
ansible all -i hosts -m ping
ansible all -i hosts -m ping -l myhost # limit selection of hosts
```

Run an arbitrary command:

```bash
ansible -i hosts -m command -a "/bin/uptime" all
ansible -i hosts -a "/bin/uptime" all # `-m command` is default and can be skipped
```

Copy a file from the control node to managed nodes:

```bash
ansible all -i hosts -m ansible.builtin.copy -a "src=./hosts dest=/tmp/hosts"
```


## Playbooks

### Examples

A minimal playbook consists of a single play, having at least:

```
- name: (optional, but recommended)
  hosts:
  tasks:
```

A simple example playbook having two plays:

```yaml
---
- name: My first play
  hosts: all
  tasks:
  - name: My first task (ping)
    ping:
  - name: My second task (copy files)
    ansible.builtin.copy:
      src: ./hosts
      dest: /tmp/hosts
- name: My second play
  hosts: onetwo
  tasks:
  - name: My first task (say hello)
    ansible.builtin.command: echo hello
  - name: My second task (ping)
    ping:
...
```

Dry-run a playbook like this:

```bash
ansible-playbook -C my_playbook.yaml -i hosts
```

Run a playbook like this:

```bash
ansible-playbook my_playbook.yaml -i hosts
```

### Playbook validation

There are different tools for different kinds of validation.

```bash
ansible-playbook --syntax-check webservers-tls.yml
ansible-lint webservers-tls.yml  # https://pypi.org/project/ansible-lint/
yamllint webservers-tls.yml
ansible-inventory --host testserver -i inventory/vagrant.ini
vagrant validate
```

Source: <https://learning.oreilly.com/library/view/ansible-up-and/9781098109141/ch03.html#validation>


### Executable playbooks

You can make playbooks executable scripts by starting them like this:

```yaml
#!/usr/bin/env ansible-playbook
# This playbook is executable as a script.
---
```

## Documentation

- Internal documentation: `ansible-doc plugin file ansible.builtin.copy`


## Quoting

### Variable reference as beginning of an inline dictionary

Bad:

```
- name: Perform some task
  command: {{ myapp }} -a foo
```

Good:

```
- name: Perform some task
  command: "{{ myapp }} -a foo"
```

### Argument containing colon

Bad:

```
- name: Show a debug message
  debug:
    msg: The debug module will print a message: neat, eh?
```

Good:

```
- name: Show a debug message
  debug:
    msg: "The debug module will print a message: neat, eh?"
```


## Loops

Example:

```
- name: Copy TLS files
  copy:
    src: "{{ item }}"
    dest: "{{ tls_dir }}"
    mode: '0600'
  loop:
    - "{{ key_file }}"
    - "{{ cert_file }}"
  notify: Restart nginx
```

Source: <https://learning.oreilly.com/library/view/ansible-up-and/9781098109141/ch03.html#loop_idulYBN5>


## Handlers

Handlers are similar to a task, but will only run when being notified by a task (when Ansible recognized that the task has changed the state of the system.)

A handler:

```
handlers:
  - name: Restart nginx
    service:
      name: nginx
      state: restarted
```

A task containing a `notify`:

```
- name: Manage nginx config template
  template:
    src: nginx.conf.j2
    dest: "{{ conf_file }}"
    mode: '0644'
  notify: Restart nginx
```

The link between task and notification happens through the handler's name (here: `Restart nginx`)

Some properties:

- Handlers run at the end of a play.
- Handlers run in the order that they are defined in the handlers section.
- Handlers only run once, even if they were notified multiple times.
- Handlers can notify other handlers (e.g. in order to validate a configuration before doing a restart.)


## References

- <https://spacelift.io/blog/ansible-tutorial>


# Ubuntu Setup

This document shows some of the steps I apply to newly installed Ubuntu systems (typically virtual machines) to make them better fit my needs. This is tested with Ubuntu 14.10 Desktop 64-bit.

## First steps

##### Change keyboard layout to Swiss (german)  
`sudo dpkg-reconfigure keyboard-configuration` -> Generic 105-key (Intl) PC -> Switzerland -> Switzerland -> default -> No compose key

##### Fix Ubuntu privacy
`wget -q -O - https://fixubuntu.com/fixubuntu.sh | bash`

## Install new things

### Install from repository

```bash
sudo apt-get update
sudo apt-get upgrade
sudo apt-get install wireshark vim colordiff tree libxml2-utils xmlstarlet dos2unix gnome-tweak-tool vlc meld git fish tmux
```

### Install from snap

```bash
snap install postman
```

### Install from pip

```bash
pip3 install tldr
```

### Install MATE

```bash
sudo apt install ubuntu-mate-core ubuntu-mate-desktop caja-open-terminal
```

`caja-open-terminal`: Right-click in file manager allows to open a terminal in this directory.

See also:

- http://wiki.mate-desktop.org/download
- https://wiki.ubuntuusers.de/MATE/

Right-click on top bar -> Add to panel -> System monitor

### Install Gnome Flashback/Fallback (Alternative to MATE)

```bash
sudo apt-get install gnome-session-fallback
```

Re-Login with "GNOME Flashback (Metacity)"

### Eclipse

Install Eclipse with file from http://www.eclipse.org

### Google Chrome

```bash
cd /tmp
wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
sudo dpkg -i google-chrome-stable_current_amd64.deb
sudo apt-get -f install
# google-chrome # run Chrome
```

Source: http://askubuntu.com/a/510063

### yt-dlp

Installation:

    python3 -m pip install -U yt-dlp
    sudo apt install ffmpeg

Update:

    python3 -m pip install -U yt-dlp

Add to path:

    echo 'set -x PATH $PATH /home/l/.local/bin' >> ~/.config/fish/config.fish
    source ~/.config/fish/config.fish

Create alias:

```fish
alias yda="yt-dlp --audio-format best --extract-audio -o '%(title)s-youtube#%(id)s.%(ext)s' --embed-thumbnail --audio-quality 0 --restrict-filenames --embed-metadata"
funcsave yda
```

## Make working more pleasant

### General

##### Configure Firefox
- Preferences -> General -> Downloads -> Always ask me where to save files
- Preferences -> Privacy -> Tracking -> Tell sites that I do not want to be tracked
- Preferences -> Privacy -> History -> Use custom settings for history -> Third-party cookies: never ; Keep until: I close Firefox
- Preferences -> Security -> [ ] Block reported attack sites ; [ ] Block reported web forgeries
- Preferences -> Advanced -> Data choices -> Disable all
- Preferences -> Advanced -> General -> Browsing -> [X] Use autoscrolling, [ ] Use smooth scrolling
- Preferences -> Advanced -> Certificates -> [ ] Query OCSP responder servers to confirm the current validity of certificates

If Firefox doesn't recognize PDF files, but instead wants to open them with VLC, open (or create) `~/.local/share/applications/defaults.list` and add the following lines:

```
[Default Applications]
application/pdf=evince.desktop;
```

Immediately load tab contents after restart: about:config -> Set `browser.sessionstore.restore_on_demand` to false (http://tabmixplus.org/support/viewpage.php?t=3&p=session-startexit)

##### Configure Terminal
- Increase scrollbuffer
- Black font on white background

##### Disable subpixel font anti-aliasing
`gnome-tweak-tool` -> Fonts -> Antialiasing -> Grayscale

##### Show console output while booting

`sudo vim /etc/default/grub`  

Find this line

    GRUB_CMDLINE_LINUX_DEFAULT="quiet splash"

And change it to:

    GRUB_CMDLINE_LINUX_DEFAULT=""

Or even more boot messages:

    GRUB_CMDLINE_LINUX_DEFAULT="no_console_suspend initcall_debug"

Then do a:

    sudo update-grub

from https://askubuntu.com/a/1137778

##### Show GRUB menu while booting

Edit `/etc/default/grub`

Make sure the lines say:

    GRUB_TIMEOUT_STYLE=menu
    GRUB_TIMEOUT=10
    GRUB_GFXMODE=640x480

##### Disable sudo password

Caution: This is a security risk, you should protect your accounts by some kind of authentication, e.g. by a harddisk encryption that needs a password at startup.

Type

    sudo visudo

At the bottom of file file add the following line

    yourusername ALL=(ALL) NOPASSWD: ALL

### MATE

##### Disable tap (on mousepad) to click

    sudo apt install xserver-xorg-input-synaptics

System -> Preferences -> Startup Applications -> Startup Programs -> Add -> `synclient MaxTapTime=0`

##### Add Applet for showing system load

Right click on top bar -> Add to panel -> System monitor

##### Auto-Login

Do

    sudo touch /etc/lightdm/lightdm.conf.d/12-autologin.conf

then add the following content:

```
[SeatDefaults]
autologin-user=yourusername
```

from https://askubuntu.com/a/456957

##### Disable screensaver / lock

Menu -> Screensaver -> [ ] Activate screensaver when computer is idle / [ ] Lock screen when screensaver is active (to be tested)

### Unity

##### Add Applet for showing system load

    sudo apt install indicator-multiload

Then search in the Dash for "System Load Indicator".

##### Disable window grouping when using Alt-Tab

    Open dconf-editor
    Go to org/gnome/desktop/wm/keybindings
    Move the value '<Alt>Tab' from switch-applications to switch-windows
    Optionally move '<Shift><Alt>Tab' from switch-applications-backward to switch-windows-backward
    If you want switch-windows to work across desktops, not just in the current desktop, you can also uncheck org/gnome/shell/window-switcher/current-workspace-only (Courtesy of @CharlBotha)
    Close dconf-editor
    If using X11, press <Alt>F2, then type r to restart Gnome.

https://superuser.com/a/860001

##### Bring back gear-wheel in case it disappeared
Alt-right-click on top bar -> Add to panel -> Indicator Applet Complete

##### Auto-Login
Gear-wheel (top right corner) -> System settings -> User accounts -> *User* -> Automatic Login

##### Disable screensaver / lock
Gear-wheel (top right corner) -> System settings -> Brightness & Lock

##### Disable automatic updates (unattended-upgrades)

`sudo -e /etc/apt/apt.conf.d/10periodic`

add the following line

`APT::Periodic::Unattended-Upgrade "0";`

Alternative: Software Center -> Edit -> Software Sources -> Tab: Updates -> Automatically check for updates: Never

Source: http://ask.xmodulo.com/disable-automatic-updates-ubuntu.html


### Configure Eclipse

##### Switch Tabs with Ctrl-Tab
Window -> Preferences -> General -> Keys -> Command: Change "Next Editor" (Ctrl-F6) to Ctrl-Tab and "Previous Editor" to Shift-Ctrl-Tab (when working with Eclipse CDT, one needs to unbind/remap the command "Toggle Source/Header", which is mapped to Ctrl-Tab by default)

##### Install Vrapper plugin
Help -> Install new software -> Work with: http://vrapper.sourceforge.net/update-site/stable/ -> Enter
Vrapper -> Choose Vrapper

##### Change font size (PyDev)

Window -> Preferences -> General -> Appearance -> Colors and Fonts -> Basic -> Text Font -> Edit ...

##### Show line numbers
Right-click to the left of the source code -> Show line numbers

##### Show 80 characters border line
Window -> Preferences -> General -> Editors -> Text Editors -> Show print margin

##### Disable spell checking
Window -> Preferences -> General -> Editors -> Text Editors -> Spelling -> [ ] Enable spell checking

##### Enable code-folding
Proceed according to http://kosiara87.blogspot.in/2011/12/how-to-install-coffee-bytes-plugin-in.html . Needs file eclipse-folding-plugin.tar.gz (MD5: 4fa41ee14d58ac3c0a50ce346d4b3daa )

##### Increase console output buffer size
Window -> Preferences -> Run/Debug -> Console -> Console buffer size

### Configure Git

##### Let there be colours
    git config --global color.ui true

##### Ignore Vim swap files

```bash
echo '.*.sw?' >> ~/.gitignore_global
git config --global core.excludesfile ~/.gitignore_global
```

##### Use Vim as Git's editor
    git config --global core.editor "vim"

##### Set default push policy
Push only the current branch and only if it has a similarly named remote tracking branch on upstream (see also http://stackoverflow.com/a/17096880 ).

    git config --global push.default simple

### Configure SSH

#### Adjust client config to prevent timeouts

Add the following to `/etc/ssh/ssh_config`:

```
# send keepalive every 30s
ServerAliveInterval 30
# assume connection is dead after 24*3600/ServerAliveInterval = 2880 tries
ServerAliveCountMax 2880
```

## Performance tweaks

### General

##### Don't save file/directory access times
In `/etc/fstab` add the options `noatime,nodiratime`.

Make sure that there are no syntax errors by remounting the changed entry, e.g. `sudo mount -o remount /`. There MUST NOT be any error messages.

##### Remove packages
     sudo apt-get autoremove -y libreoffice-* transmission-* deja-dup zeitgeist bluez brasero rhythmbox gnome-screensaver apport thunderbird unity-webapps-common

The wisdom of the Internet said that the following packets can also be removed:

     gnome-mahjongg gnome-sudoku gnome-orca aisleriot gnomine ttf-indic-fonts-core ppp brltty brltty-x11 gnome-accessibility-themes espeak espeak-data libespeak1 libgnome-speech7 evolution-common evolution-data-server evolution-plugins totem

However, the removal of one of these probably caused the problem, that clicking gear-wheel -> System Settings... won't display anything anymore. The corresponding window can be displayed using `gnome-control-center` but is missing some elements. The situation seems identical or very similar to https://askubuntu.com/questions/453440/missing-system-settings-after-removing-some-packages . The thread mentions `evolution*` packages and a fix: `sudo apt-get install unity-control-center --no-install-recommends` which seems to work.

##### Manage applications that are autostarted

Menu -> Startup applications

### MATE

##### Disable dropshadows / animations

    gsettings set org.mate.Marco.general compositing-manager false # https://wiki.archlinux.org/index.php/MATE#Disabling_compositing

##### Lock screen at suspend, etc.

```bash
sudo apt install xss-lock
```

Then -> Start menu -> Startup Applications -> Add -> Name: "Screen Locker", Command: "xss-lock -- mate-screensaver-command --lock"

### Unity

##### Disable border shadows
    gsettings set org.gnome.metacity compositing-manager false

##### Disable animated logos when clicking on shortcuts in the top taskbar
The animation can be disabled with `gsettings set org.gnome.desktop.interface enable-animations false`. Unfortunately the setting is not permanent. http://forum.ubuntuusers.de/topic/gnome-animationen-deaktivieren/ recommends:

Start `gnome-session-properties` and put the above command into auto start.

##### Fix high CPU load on VirtualBox

Source: https://askubuntu.com/questions/207813/why-does-a-ubuntu-guest-in-virtualbox-run-very-very-slowly/214968#214968

- `sudo apt-get install linux-headers-$(uname -r) build-essential`
- Install Guest Additions
- Add the line `vboxvideo` to the file `/etc/modules`
- Shutdown the virtual machine
- VM settings -> Display -> [X] Enable 3D acceleration
- Check: `/usr/lib/nux/unity_support_test -p` should report all yes

## Special actions

### Automatically type LUKS password (removes cryptographical security!)

Add this at the end of the relevant line in `/etc/crypttab`:

    ,keyscript=/root/lukspw

Create the file /root/lukspw with the content:

    #!/bin/sh
    sleep 10
    printf "password"

And make it executable:

    chmod +x /root/lukspw

And update the initrd (assuming bash):

    update-initramfs -u -k`uname -r`

Source: <http://atterer.org/linux-remove-disable-luks-encryption-password-on-disk-partition-crypttab-initrd>

If this breaks your machine, maybe <https://feeding.cloud.geek.nz/posts/recovering-from-unbootable-ubuntu-encrypted-lvm-root-partition/> can help...

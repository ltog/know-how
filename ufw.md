# ufw

This page describes the usage of `ufw`, the uncomplicated firewall for Linux.

## General commands

Check wether ufw runs:

    sudo ufw status

Start ufw and enable autostart:

    sudo ufw enable

Stop ufw and disable autostart:

    sudo ufw disable

View ufw rules (ufw must run for this):

    sudo ufw status verbose

## Adding rules

Newly added rules are active immediately, however, existing connections WILL NOT be interrupted.

If you are blocking outgoing connections, ping (ICMP) will be blocked as well.

### Normalization of network addresses

In order to specify subnets you don't have to use the network address necessarily. You can use any IP address of the corresponding range.

## Removing rules

Show the numbered list of rules:

    sudo ufw status numbered

Delete a single rule based on its number:

    sudo ufw delete NUMBER_OF_RULE

Note that this will assign new numbers to the remaining rules. You will have to rerun the above command.

## Note about reject/deny

There are two commands to block traffic:

- **deny:** block traffic WITHOUT notifying sender
- **reject:** block traffic WITH notifying sender (using ICMP?)

## Example configuration

sudo apt update
sudo apt install ufw
sudo ufw default deny incoming
sudo ufw default allow outgoing
sudo ufw allow from 192.168.1.0/16 comment 'local network'
sudo ufw allow from 198.111.13.16 comment 'allow some specific address'
sudo ufw enable

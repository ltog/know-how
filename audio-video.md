# Audio/Video

## Remove Audio stream from video

    avconv -i input.avi -an -c:v copy output.avi

(Source: http://ubuntuforums.org/showthread.php?t=2084457&s=f0e0024253bf6293f84a00c1eaa54a44&p=12356277#post12356277 , http://www.commandlinefu.com/commands/view/10992/remove-audio-stream-from-a-media-file)

    ffmpeg -i input.avi -vcodec copy -an output.avi

(Source: http://superuser.com/a/484860 )

## Crop away beginning and end from video

Source: https://superuser.com/questions/377343/cut-part-from-video-file-from-start-position-to-end-position-with-ffmpeg , http://www.dototot.com/how-to-trim-videos-with-the-command-line-using-avconv/

### Accurate method

    avconv -i input.avi -ss 00:00:42 -t 00:03:26 -codec copy output.avi 

Parameters:

- `-ss`: Start time
- `-t`: Duration

## Convert to h264/.mp4

    avconv -i input.avi -c:v libx264 output.mp4

Source: http://stackoverflow.com/a/18979552

## Prepare for upload to Mapillary (remove audio, convert videoformat)

    avconv -i input.avi -an -c:v libx264 output.mp4

## Change container format

    ffmpeg -i input.mp4 -c copy -map 0 output.mkv

or

    avconv -i input.mp4 -c copy -map 0 output.mkv

Source: https://superuser.com/questions/378726/convert-from-mov-to-mp4-container-format

## Convert to open format (webm container with VP9 video codec and Opus audio codec)

ffmpeg -i input.mp4 -c:v libvpx-vp9 -b:v 0 -crf 30 -pass 1 -row-mt 1 -an -f null /dev/null && \
ffmpeg -i input.mp4 -c:v libvpx-vp9 -b:v 0 -crf 30 -pass 2 -row-mt 1 -c:a libopus -deadline best output.webm

Optional parameters for the second pass:
- `-b:a 128k`: set bitrate to 128kbit/s (default is 96)
- `-ar 44100`: set sample rate (if higher than 44100)

Example:

ffmpeg -i prohombrechtikon.avi -c:v libvpx-vp9 -b:v 0 -crf 30 -pass 1 -row-mt 1 -an -f null /dev/null && ffmpeg -i prohombrechtikon.avi -c:v libvpx-vp9 -b:v 0 -crf 30 -pass 2 -row-mt 1 -ar 44100 -b:a 128k -c:a libopus -deadline best output.webm

Some notes:

- default opus bitrate (`-b:a`) is 96kbit/s
- `-deadline`: `best`, `good` (default), `realtime`
- CRF value can be from 0 to 63, lower means better quality. Recommended values from 15 to 35, with 31 being recommended for 1080p HD videos.

Recommendations from Google:

| Frame Height | Target Quality (CQ) |
|--------------|---------------------|
| 240          | 37                  |
| 360          | 36                  |
| 480          | 34 (LQ) or 33 (MQ)  |
| 720          | 32                  |
| 1080         | 31                  |
| 1440         | 24                  |
| 2160         | 15                  |

Sources:

- <https://trac.ffmpeg.org/wiki/Encode/VP9>
- <https://developers.google.com/media/vp9/settings/vod/>

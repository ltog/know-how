# screen

## Handling sessions

Create session:

    screen

Detach session:

    Ctrl-a d

Re-attach session:

    screen -r

Kill session:

    Ctrl-a k

## Work with screen

Scroll back:

    Ctrl-a [  # quit with Esc

## Using the Cisco serial cable

Determine the port:

    # attach cable to USB port
    dmesg | grep -i tty  # e.g. ttyUSB0

Connect:

    screen /dev/ttyUSB0

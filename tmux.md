# tmux

## Basic operations

Create new session:

    tmux # session will have the name '0'

or 

    tmux new -s <session>

Detach sesssion (it will continue to run in the background):

    Ctrl-b d

Re-attach (= resume) session:

    tmux attach -t <session>

List sessions:

    tmux ls

Kill session:

    tmux kill-session -t <session>

# Scrolling 

Begin scrolling:

    Ctrl-b [

End scrolling:

    q

## Configuration settings

Put this into `~/.tmux.conf`:

```
# Allow more room for scrolling
set -g history-limit 30000
```

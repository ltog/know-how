# fish

## Setup

Use fish as default shell:

    chsh -s `which fish` # https://askubuntu.com/a/26440

Remove greeting:

    set fish_greeting # https://stackoverflow.com/a/13995944

Open config in browser:

    fish_config

Disable SVN prompt (only needed on old fish versions): `funced fish_vcs_prompt` -> Add `#` in front of `or fish_svn_prompt`

(Source: <https://github.com/fish-shell/fish-shell/commit/c37a4258872eac809d9591dc4d67a03c52789ba1>)

## Environment variables

Set a variable for one command only:

```fish
BLUB=blub echo $BLUB
```

Export a variable into sub-processes:

```fish
set -x SOMEVAR somevalue  # equivalent to bash's `export SOMEVAR=somevalue`
```

Set a variable across reboots:

```fish
set -Ux MY_VARIABLE my_value
```

Show variables:

```fish
set -S
```

## stdout / stderr

Combine stdout and stderr:

    cmd1 &| cmd2

## Aliases

Define an alias:

    alias home="cd /home/bozo"
    funcsave home

from https://stackoverflow.com/a/2763014

Remove an alias:

    functions -e myalias

## Deleting history entries

This may be desired if you typed your password into the shell:

```fish
history delete -C MySecretPassword
history delete -C MySecretPassword # type it again to delete the delete command
reset
exit
```

## Links

- <https://hyperpolyglot.org/unix-shells>

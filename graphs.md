#!/usr/bin/env python3

# Working with graphs

## Finding cycles in a graph

```python3
from networkx import *

g = DiGraph()

g.add_edge('b', 'a')
g.add_edge('c', 'b')
g.add_edge('c', 'a')
g.add_edge('a', 'd')
g.add_edge('d', 'e')
g.add_edge('e', 'c')

for g in simple_cycles(g):
    print(g)

# ['a', 'd', 'e', 'c']
# ['a', 'd', 'e', 'c', 'b']
```

See <https://networkx.org/documentation/stable/reference/algorithms/generated/networkx.algorithms.cycles.simple_cycles.html>


## Print all nodes reachable from a given node

```python3
from networkx import *

g = DiGraph()

g.add_edge(1,2)
g.add_edge(2,3)
g.add_edge(3,4)
g.add_edge(3,5)
g.add_edge(5,6)
g.add_edge(4,3) # creates a cycle

for v in bfs_tree(g, 2):
    print(v)

# 2
# 3
# 4
# 5
# 6
```

See <https://networkx.org/documentation/stable/reference/algorithms/generated/networkx.algorithms.traversal.breadth_first_search.bfs_tree.html>

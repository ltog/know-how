# Python Know How

## Python language and modules

### Input & Output

Printing to stderr:

```python3
import sys
print("This is an error message", file=sys.stderr)
```

### Basic datastructures

#### List

- Create:
  - `list()`
  - `[]`
  - `[1, 2, "a", "b"]`
- Add item:
  - `append(item)` (single item)`
  - `my_list += item` (like `append()`)
  - `insert(idx, item)` (insert item at specified position)
- Add item from other list/iterable:
  - `extend(items)` (take items from iterable, e.g. other list)
- Remove item:
  - `remove()` (remove first occurrence)
  - `pop()` or `pop(idx)`
  - `del my_list[idx]` (remove without returning)
- Remove all items:
  - `clear()` (make list empty)

#### Tuple

#### Set

- Create:
  - `set()`
- Add item:
  - `s.add(item)`
- Add items from other set:
  - `s.update(other_set)`
- Remove item:
  - `s.remove(item)` (raises `KeyError` if item is not in set)
  - `s.discard(item)` (does not raise error if item is not in set)
- Remove arbitrary item:
  - `s.pop()` (raises `KeyError if set is empty)
- Remove all items:
  - `s.clear()`

#### Dictionary

- Create:
  - `{}`
  - `dict()`
  - `{'key1':'val1','key2':'val2'}`
- Iterate:
  - over keys: `for k in dict_`
  - over values: `for v in d.values()`
  - over keys and values: `for k, v in d.items()`
- Add item:
  - `d['k1'] = 'v1'`
  - `d.update({'k1': 'v1', 'k2': 'v2'})`
- Remove item:
  - `del d['k1']` (raises `KeyError` if key does not exist)


### NOOP function

```python
noop = lambda *a, **k: None
noop()
```

Source: https://stackoverflow.com/a/44504044

### Comprehensions

#### List comprehensions

Squared numbers:

```python
squares = [x**2 for x in range(10)]
```

Comprehension with if statement (general form):

```python
[f(x) for x in sequence if condition]
```

Comprehension with if statement (example):
```python
[x for x in range(10) if x%2==0]
```

Comprehension with if-else statement (general form):

```python
[f(x) if condition else g(x) for x in sequence]
```

Comprehension with if-else statement (example):

```python
['even' if x%2==0 else 'odd' for x in range(10)]
```

Source: https://stackoverflow.com/a/4260304

#### Dict comprehension

Modify values:

```python
dict1 = {'a': 1, 'b': 2, 'c': 3, 'd': 4, 'e': 5}
double_values = {k:v*2 for (k,v) in dict1.items()}
# {'e': 10, 'a': 2, 'c': 6, 'b': 4, 'd': 8}
```

Modify keys:

```python
dict1 = {'a': 1, 'b': 2, 'c': 3, 'd': 4, 'e': 5}
double_keys = {k*2:v for (k,v) in dict1.items()}
# {'dd': 4, 'ee': 5, 'aa': 1, 'bb': 2, 'cc': 3}
```

if statement:

```python
dict1 = {'a': 1, 'b': 2, 'c': 3, 'd': 4, 'e': 5}
dict1_cond = {k:v for (k,v) in dict1.items() if v>2}
# {'e': 5, 'c': 3, 'd': 4}
```

Multiple if statements (combined with AND):

```python
dict1 = {'a': 1, 'b': 2, 'c': 3, 'd': 4, 'e': 5}
dict1_doubleCond = {k:v for (k,v) in dict1.items() if v>2 if v%2 == 0}
# {'d': 4}
```

if-else statement:

```python
dict1 = {'a': 1, 'b': 2, 'c': 3, 'd': 4, 'e': 5, 'f':6}
dict1_tripleCond = {k:('even' if v%2==0 else 'odd') for (k,v) in dict1.items()}
# {'f': 'even', 'c': 'odd', 'b': 'even', 'd': 'even', 'e': 'odd', 'a': 'odd'}
```

Source: https://www.datacamp.com/community/tutorials/python-dictionary-comprehension

### multiprocessing

#### Pool

Comparison of methods:


| function           | multi-args | concurrency | blocking | ordered results | notes                                                                                                                             |
|--------------------|------------|-------------|----------|-----------------|-----------------------------------------------------------------------------------------------------------------------------------|
| `apply()`          | yes        | no          | yes      | no              | Arbitrary worker runs given function, waits for it, returns result. Runs only *one* job. Internally uses `apply_async(...).get()` |
| `apply_async()`    | yes        | yes         | no       | no              | Arbitrary worker runs given function, immediately returns `AsyncResult`, optionally calls callback. Runs only *one* job.          |
| `imap()`           | no         | yes         | yes      | yes             | First task is returned as soon as it finished.                                                                                    |
| `imap_unordered()` | no         | yes         | yes      | no              | Shortest task is returned as soon as it finished, potentially sooner than with `imap()`.                                          |
| `map()`            | no         | yes         | yes      | yes             | No usable results until longest job has finished.                                                                                 |
| `map_async()`      | no         | yes         | no       | yes             |                                                                                                                                   |
| `starmap()`        | yes        | yes         | yes      | yes             | Like `map()` but with multiple arguments.                                                                                         |
| `starmap_async()`  | yes        | yes         | no       | yes             | Like `map_async()` but with multiple arguments.                                                                                   |

How to choose a method for parallel computing of a certain task:

1. Decide if you want to use a synchronous or asynchronous (table: blocking=no) method. Synchronous methods are usually easier.
2. Note that `apply()` and `apply_async()` aren't really suitable for parallelizing computations.
3. Choose `map()` or `map_async()` except for the following special cases.
4. Use `imap()` or `imap_unordered()` in the following cases:
  - If converting your iterable to a list (done automatically by `map()` and `map_async()`) uses too much memory, .
  - If you want to be able to process the first results even though some processes are still running.
    - `imap()` returns the first task as soon as it finished.
    - `imap_unordered()` returns the shortest task as soon as it finished. That may be earlier than the first result by `imap()`.
6. Use `starmap()` or `starmap_async()` if you want to parallelize functions with multiple arguments and are on Python 3.3 or later. Note that there are workarounds to use the other methods with multiple arguments.

Sources:

- <http://blog.shenwei.me/python-multiprocessing-pool-difference-between-map-apply-map_async-apply_async/>
- <https://stackoverflow.com/a/26521507/2422702>

## Python tools

### pytest

Running only some tests:

```bash
pytest tests/test_my_class.py::test_my_func tests/test_my_other_class.py
```

### pyenv

An introduction: <https://realpython.com/intro-to-pyenv/>

#### Installing pyenv

```bash
sudo apt-get install -y make build-essential libssl-dev zlib1g-dev libbz2-dev libreadline-dev libsqlite3-dev wget curl llvm libncurses5-dev libncursesw5-dev xz-utils tk-dev libffi-dev liblzma-dev python-openssl
curl https://pyenv.run | bash # this uses https://github.com/pyenv/pyenv-installer, or manually run the steps at https://github.com/pyenv/pyenv#basic-github-checkout
```


#### Handling Python versions

Install a specific Python version:

```bash
pyenv install 3.7.2
# or
pyenv install -v 3.7.2 # more verbose
```

Uninstall a specific Python version:

```bash
pyenv uninstall 3.7.2
```

Show available Python versions:

```bash
pyenv versions
```

Show currently used Python version:

```bash
pyenv which python
```

#### Handling virtualenvs

Note about priorities in resolving the python command: Priorities are resolved from top to bottom:

- `pyenv shell` (defined by $PYENV_VERSION)
- `pyenv local` (defined by `.python-version` file)
- `pyenv global` (defined by `~/.pyenv/version` file)
- System Python

Define virtualenv for a specific directory/project. The correct virtualenv will automatically be (de)activated when cd-ing into/out of the project directory:

```bash
pyenv virtualenv 3.6.10 my-venv-3.6.10 # create virtualenv
cd myproject
pyenv local my-venv-3.6.10
pyenv which python # make sure correct binary is used
```

List virtualenvs:

```bash
pyenv virtualenvs
```

Delete virtualenv:

```bash
pyenv virtualenv-delete doomed-virtualenv
```

## Libraries

### tenacity

#### Make sure exponential backoff doesn't try after specified time.

Summary: Don't use `stop_after_delay`, but use instead a modified value of `(desired_limit - multiplier)/2`. Reason: Tenacity will evaluate the `stop_after_delay` value only after an execution but not while waiting for the next execution.

```python
#!/usr/bin/env python3

from tenacity import Retrying, stop_after_delay, wait_exponential, RetryError, retry
import time

ts = time.monotonic()

try:
    no_retries_after = 9.02
    multiplier = 3
    for attempt in Retrying(stop=stop_after_delay((no_retries_after-multiplier)/2), wait=wait_exponential(multiplier=multiplier)):
        with attempt:
            now = time.monotonic()
            diff = now - ts
            print(f'diff={diff}')
            raise Exception('This is my exception that causes tenacity to retry.')
except RetryError:
    pass
```



## Debugging

### Debugging interactively using pdb

Put the following line where you want to debug:

```python
import pdb; pdb.set_trace()
```

You can then use the following commands:

- `bt`: Print backtracke
- `up`: Move scope along the function call stack to the caller of the current function
- `down`: Move scope to the callee of the current function
- `step`: "step into"
- `next`: "step over"
- `return`: "step out"
- `continue`: Continue program until next breakpoint or set_trace() call


## Use cases covered with Python

### Serving websites the simple way

`python3 -m http.server`

If having CORS-related problems when loading external resources, create a new file `simple-server.py`:

```python
#!/usr/bin/env python3

# from https://stackoverflow.com/questions/21956683/enable-access-control-on-simple-http-server

from http.server import HTTPServer, SimpleHTTPRequestHandler, test
import sys

class CORSRequestHandler (SimpleHTTPRequestHandler):
    def end_headers (self):
        self.send_header('Access-Control-Allow-Origin', '*')
        SimpleHTTPRequestHandler.end_headers(self)

if __name__ == '__main__':
    test(CORSRequestHandler, HTTPServer, port=int(sys.argv[1]) if len(sys.argv) > 1 else 8000)
```

then run `python3 simple-server.py`. Files will be available at <http://localhost:8000/>.
